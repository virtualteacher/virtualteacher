create table virtual_teacher.assignments
(
    assignment_id int auto_increment
        primary key,
    name varchar(100) not null,
    file_path varchar(255) null
);

create table virtual_teacher.homeworks
(
    homework_id int auto_increment
        primary key,
    name varchar(30) not null,
    file_path varchar(255) not null,
    user_email varchar(30) not null,
    assignment_id int not null,
    evaluation double default 0 not null,
    constraint homeworks_assignments_assignment_id_fk
        foreign key (assignment_id) references virtual_teacher.assignments (assignment_id)
);

create table virtual_teacher.lectures
(
    lecture_id int auto_increment
        primary key,
    title varchar(50) not null,
    description varchar(1000) null,
    video_url varchar(255) null
);

create table virtual_teacher.lectures_assignments
(
    lecture_id int not null,
    assignment_id int null,
    constraint lectures_assignments_assignments_assignment_id_fk
        foreign key (assignment_id) references virtual_teacher.assignments (assignment_id),
    constraint lectures_assignments_lectures_lecture_id_fk
        foreign key (lecture_id) references virtual_teacher.lectures (lecture_id)
);

create table virtual_teacher.roles
(
    role_id int auto_increment
        primary key,
    name varchar(15) not null
);

create table virtual_teacher.statuses
(
    status_id int auto_increment
        primary key,
    type varchar(10) not null
);

create table virtual_teacher.topics
(
    topic_id int auto_increment
        primary key,
    name varchar(20) not null
);

create table virtual_teacher.courses
(
    course_id int auto_increment
        primary key,
    title varchar(50) not null,
    topic_id int not null,
    description varchar(1000) null,
    starting_date date not null,
    rating double not null,
    status_id int not null,
    constraint courses_statuses_status_id_fk
        foreign key (status_id) references virtual_teacher.statuses (status_id),
    constraint courses_topics_topic_id_fk
        foreign key (topic_id) references virtual_teacher.topics (topic_id)
);

create table virtual_teacher.courses_lectures
(
    course_id int not null,
    lecture_id int not null,
    constraint table_name_courses_course_id_fk
        foreign key (course_id) references virtual_teacher.courses (course_id),
    constraint table_name_lectures_lecture_id_fk
        foreign key (lecture_id) references virtual_teacher.lectures (lecture_id)
);

create table virtual_teacher.users
(
    user_id int auto_increment
        primary key,
    first_name varchar(20) not null,
    last_name varchar(20) not null,
    email varchar(30) not null,
    password varchar(20) not null,
    profile_picture_url varchar(255) default '../assets/img/no-avatar.png' null,
    constraint users_email_uindex
        unique (email)
);

create table virtual_teacher.courses_ratings
(
    id int auto_increment
        primary key,
    user_Id int not null,
    course_id int not null,
    rating double not null,
    constraint courses_ratings_courses_course_id_fk
        foreign key (course_id) references virtual_teacher.courses (course_id),
    constraint courses_ratings_users_user_id_fk
        foreign key (user_Id) references virtual_teacher.users (user_id)
);

create table virtual_teacher.courses_teachers
(
    course_id int not null,
    user_id int not null,
    constraint courses_teachers_courses_course_id_fk
        foreign key (course_id) references virtual_teacher.courses (course_id),
    constraint courses_teachers_users_user_id_fk
        foreign key (user_id) references virtual_teacher.users (user_id)
);

create table virtual_teacher.users_courses
(
    user_id int not null,
    course_id int not null,
    constraint users_courses_courses_course_id_fk
        foreign key (course_id) references virtual_teacher.courses (course_id),
    constraint users_courses_users_user_id_fk
        foreign key (user_id) references virtual_teacher.users (user_id)
);

create table virtual_teacher.users_roles
(
    user_id int not null,
    role_id int not null,
    constraint user_roles_roles_role_id_fk
        foreign key (role_id) references virtual_teacher.roles (role_id),
    constraint user_roles_users_user_id_fk
        foreign key (user_id) references virtual_teacher.users (user_id)
);

