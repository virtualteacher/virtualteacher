-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия на сървъра:            10.6.3-MariaDB - mariadb.org binary distribution
-- ОС на сървъра:                Win64
-- HeidiSQL Версия:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Дъмп на структурата на БД virtual_teacher
CREATE DATABASE IF NOT EXISTS `virtual_teacher` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `virtual_teacher`;

-- Дъмп структура за таблица virtual_teacher.assignments
CREATE TABLE IF NOT EXISTS `assignments` (
    `assignment_id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(100) NOT NULL,
    `file_path` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`assignment_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица virtual_teacher.assignments: ~3 rows (приблизително)
DELETE FROM `assignments`;
/*!40000 ALTER TABLE `assignments` DISABLE KEYS */;
INSERT INTO `assignments` (`assignment_id`, `name`, `file_path`) VALUES
(7, 'First steps into Java - Assignment', '/uploads/1633630606750_First steps into Java - Assignment'),
(8, 'First steps into Python - Assignment', '/uploads/1633630646344_First steps into Python - Assignment'),
(9, 'Introduction to Physics - Assignment', '/uploads/1633675059041_Introduction to Physics - Assignment');
/*!40000 ALTER TABLE `assignments` ENABLE KEYS */;

-- Дъмп структура за таблица virtual_teacher.courses
CREATE TABLE IF NOT EXISTS `courses` (
    `course_id` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(50) NOT NULL,
    `topic_id` int(11) NOT NULL,
    `description` varchar(1000) DEFAULT NULL,
    `starting_date` date NOT NULL,
    `rating` double NOT NULL,
    `status_id` int(11) NOT NULL,
    PRIMARY KEY (`course_id`),
    KEY `courses_topics_topic_id_fk` (`topic_id`),
    KEY `courses_statuses_status_id_fk` (`status_id`),
    CONSTRAINT `courses_statuses_status_id_fk` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`status_id`),
    CONSTRAINT `courses_topics_topic_id_fk` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`topic_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица virtual_teacher.courses: ~5 rows (приблизително)
DELETE FROM `courses`;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` (`course_id`, `title`, `topic_id`, `description`, `starting_date`, `rating`, `status_id`) VALUES
(1, 'Python Basics', 1, 'Getting started with Python', '2021-10-08', 0, 2),
(7, 'Java Basics', 1, 'Getting started with Java', '2021-10-21', 0, 2),
(8, 'C# Basics', 1, 'Getting started with C#', '2021-10-25', 0, 1),
(10, 'Mathematics - Analytic Geometry', 2, 'Analytic geometry - Part 1', '2021-10-26', 0, 1),
(12, 'Physics basics', 3, 'Physics basic definitions and concepts.', '2021-10-12', 0, 2);
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;

-- Дъмп структура за таблица virtual_teacher.courses_lectures
CREATE TABLE IF NOT EXISTS `courses_lectures` (
    `course_id` int(11) NOT NULL,
    `lecture_id` int(11) NOT NULL,
    KEY `table_name_courses_course_id_fk` (`course_id`),
    KEY `table_name_lectures_lecture_id_fk` (`lecture_id`),
    CONSTRAINT `table_name_courses_course_id_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
    CONSTRAINT `table_name_lectures_lecture_id_fk` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`lecture_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица virtual_teacher.courses_lectures: ~3 rows (приблизително)
DELETE FROM `courses_lectures`;
/*!40000 ALTER TABLE `courses_lectures` DISABLE KEYS */;
INSERT INTO `courses_lectures` (`course_id`, `lecture_id`) VALUES
(7, 14),
(1, 15),
(12, 18);
/*!40000 ALTER TABLE `courses_lectures` ENABLE KEYS */;

-- Дъмп структура за таблица virtual_teacher.courses_ratings
CREATE TABLE IF NOT EXISTS `courses_ratings` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_Id` int(11) NOT NULL,
    `course_id` int(11) NOT NULL,
    `rating` double NOT NULL,
    PRIMARY KEY (`id`),
    KEY `courses_ratings_courses_course_id_fk` (`course_id`),
    KEY `courses_ratings_users_user_id_fk` (`user_Id`),
    CONSTRAINT `courses_ratings_courses_course_id_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
    CONSTRAINT `courses_ratings_users_user_id_fk` FOREIGN KEY (`user_Id`) REFERENCES `users` (`user_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица virtual_teacher.courses_ratings: ~0 rows (приблизително)
DELETE FROM `courses_ratings`;
/*!40000 ALTER TABLE `courses_ratings` DISABLE KEYS */;
/*!40000 ALTER TABLE `courses_ratings` ENABLE KEYS */;

-- Дъмп структура за таблица virtual_teacher.courses_teachers
CREATE TABLE IF NOT EXISTS `courses_teachers` (
    `course_id` int(11) NOT NULL,
    `user_id` int(11) NOT NULL,
    KEY `courses_teachers_courses_course_id_fk` (`course_id`),
    KEY `courses_teachers_users_user_id_fk` (`user_id`),
    CONSTRAINT `courses_teachers_courses_course_id_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
    CONSTRAINT `courses_teachers_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица virtual_teacher.courses_teachers: ~6 rows (приблизително)
DELETE FROM `courses_teachers`;
/*!40000 ALTER TABLE `courses_teachers` DISABLE KEYS */;
INSERT INTO `courses_teachers` (`course_id`, `user_id`) VALUES
(7, 2),
(1, 2),
(1, 4),
(8, 4),
(10, 4),
(12, 4);
/*!40000 ALTER TABLE `courses_teachers` ENABLE KEYS */;

-- Дъмп структура за таблица virtual_teacher.homeworks
CREATE TABLE IF NOT EXISTS `homeworks` (
    `homework_id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(30) NOT NULL,
    `file_path` varchar(255) NOT NULL,
    `user_email` varchar(30) NOT NULL,
    `assignment_id` int(11) NOT NULL,
    `evaluation` double NOT NULL DEFAULT 0,
    PRIMARY KEY (`homework_id`),
    KEY `homeworks_assignments_assignment_id_fk` (`assignment_id`),
    CONSTRAINT `homeworks_assignments_assignment_id_fk` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`assignment_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица virtual_teacher.homeworks: ~0 rows (приблизително)
DELETE FROM `homeworks`;
/*!40000 ALTER TABLE `homeworks` DISABLE KEYS */;
/*!40000 ALTER TABLE `homeworks` ENABLE KEYS */;

-- Дъмп структура за таблица virtual_teacher.lectures
CREATE TABLE IF NOT EXISTS `lectures` (
    `lecture_id` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(50) NOT NULL,
    `description` varchar(1000) DEFAULT NULL,
    `video_url` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`lecture_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица virtual_teacher.lectures: ~5 rows (приблизително)
DELETE FROM `lectures`;
/*!40000 ALTER TABLE `lectures` DISABLE KEYS */;
INSERT INTO `lectures` (`lecture_id`, `title`, `description`, `video_url`) VALUES
(14, 'First Steps into Java', 'Java is one of the most popular programming languages out there. Released in 1995 and still widely used today, Java has many applications, including software development, mobile applications, and large systems development. Knowing Java opens a lot of possibilities for you as a developer. In this lecture we will show you how to install IntelliJ.', 'https://www.youtube.com/embed/EMLTOMdIz4w'),
(15, 'First steps into Python', 'Welcome! Are you completely new to programming? If not then we presume you will be looking for information about why and how to get started with Python. Fortunately an experienced programmer in any programming language (whatever it may be) can pick up Python very quickly. It\'s also easy for beginners to use and learn, so jump in! In this lecture we will show you how to install PyCharm.', 'https://www.youtube.com/embed/SZUNUB6nz3g'),
	(16, 'First steps into C#', 'C# is a general-purpose, modern and object-oriented programming language pronounced as “C sharp”. It was developed by Microsoft led by Anders Hejlsberg and his team within the .Net initiative and was approved by the European Computer Manufacturers Association (ECMA) and International Standards Organization (ISO).In this lecture we will show you how to install VisualStudio.', 'https://www.youtube.com/embed/Bu26MZAv12E'),
	(17, 'What is Analytic Geometry', 'In classical mathematics, analytic geometry, also known as coordinate geometry, or Cartesian geometry, is the study of geometry using a coordinate system. This contrasts with synthetic geometry. Analytic geometry is widely used in physics and engineering, and is the foundation of most modern fields of geometry, including algebraic, differential, discrete and computational geometry.', 'https://www.youtube.com/embed/HbhjlgjdTEA'),
	(18, 'Introduction to Physics', 'In this lesson, you will learn an introduction to physics and the important concepts and terms associated with physics 1 at the high school, college, or university level.', 'https://www.youtube.com/embed/i_CijGuk7fw');
/*!40000 ALTER TABLE `lectures` ENABLE KEYS */;

-- Дъмп структура за таблица virtual_teacher.lectures_assignments
CREATE TABLE IF NOT EXISTS `lectures_assignments` (
  `lecture_id` int(11) NOT NULL,
  `assignment_id` int(11) DEFAULT NULL,
  KEY `lectures_assignments_assignments_assignment_id_fk` (`assignment_id`),
  KEY `lectures_assignments_lectures_lecture_id_fk` (`lecture_id`),
  CONSTRAINT `lectures_assignments_assignments_assignment_id_fk` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`assignment_id`),
  CONSTRAINT `lectures_assignments_lectures_lecture_id_fk` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`lecture_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица virtual_teacher.lectures_assignments: ~1 rows (приблизително)
DELETE FROM `lectures_assignments`;
/*!40000 ALTER TABLE `lectures_assignments` DISABLE KEYS */;
INSERT INTO `lectures_assignments` (`lecture_id`, `assignment_id`) VALUES
	(18, 9);
/*!40000 ALTER TABLE `lectures_assignments` ENABLE KEYS */;

-- Дъмп структура за таблица virtual_teacher.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица virtual_teacher.roles: ~3 rows (приблизително)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `name`) VALUES
	(1, 'Administrator'),
	(2, 'Teacher'),
	(3, 'Student');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Дъмп структура за таблица virtual_teacher.statuses
CREATE TABLE IF NOT EXISTS `statuses` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица virtual_teacher.statuses: ~2 rows (приблизително)
DELETE FROM `statuses`;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` (`status_id`, `type`) VALUES
	(1, 'Draft'),
	(2, 'Published');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;

-- Дъмп структура за таблица virtual_teacher.topics
CREATE TABLE IF NOT EXISTS `topics` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица virtual_teacher.topics: ~4 rows (приблизително)
DELETE FROM `topics`;
/*!40000 ALTER TABLE `topics` DISABLE KEYS */;
INSERT INTO `topics` (`topic_id`, `name`) VALUES
	(1, 'Software Development'),
	(2, 'Mathematics'),
	(3, 'Physics'),
	(4, 'Chemistry');
/*!40000 ALTER TABLE `topics` ENABLE KEYS */;

-- Дъмп структура за таблица virtual_teacher.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `profile_picture_url` varchar(255) DEFAULT '../assets/img/no-avatar.png',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_email_uindex` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица virtual_teacher.users: ~4 rows (приблизително)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `password`, `profile_picture_url`) VALUES
	(2, 'Boris', 'Grozev', 'bgrozev@email.com', 'Password2!', '/uploads/1633620404326_photo'),
	(4, 'Petar', 'Raykov', 'praykov@email.com', 'password3', '../assets/img/no-avatar.png'),
	(5, 'Todor', 'Andonov', 'todor@email.com', 'Password1@', '../assets/img/no-avatar.png'),
	(14, 'Vladislav', 'Matsinski', 'vmacinski@abv.bg', 'Password@@1', '/uploads/1633619613172_photo');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Дъмп структура за таблица virtual_teacher.users_courses
CREATE TABLE IF NOT EXISTS `users_courses` (
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  KEY `users_courses_courses_course_id_fk` (`course_id`),
  KEY `users_courses_users_user_id_fk` (`user_id`),
  CONSTRAINT `users_courses_courses_course_id_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
  CONSTRAINT `users_courses_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица virtual_teacher.users_courses: ~0 rows (приблизително)
DELETE FROM `users_courses`;
/*!40000 ALTER TABLE `users_courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_courses` ENABLE KEYS */;

-- Дъмп структура за таблица virtual_teacher.users_roles
CREATE TABLE IF NOT EXISTS `users_roles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  KEY `user_roles_roles_role_id_fk` (`role_id`),
  KEY `user_roles_users_user_id_fk` (`user_id`),
  CONSTRAINT `user_roles_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  CONSTRAINT `user_roles_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица virtual_teacher.users_roles: ~5 rows (приблизително)
DELETE FROM `users_roles`;
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` (`user_id`, `role_id`) VALUES
	(5, 3),
	(4, 2),
	(2, 2),
	(2, 1),
	(14, 3);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
