package com.example.virtualteacher.controllers.rest;

import com.example.virtualteacher.controllers.AuthenticationHelper;
import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.exceptions.UnauthorisedOperationException;
import com.example.virtualteacher.models.Assignment;
import com.example.virtualteacher.models.Homework;
import com.example.virtualteacher.models.Lecture;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.models.dtos.AssignmentDto;
import com.example.virtualteacher.models.dtos.EvaluateHomeworkDto;
import com.example.virtualteacher.models.dtos.HomeworkDto;
import com.example.virtualteacher.services.contracts.AssignmentService;
import com.example.virtualteacher.services.contracts.HomeworkService;
import com.example.virtualteacher.services.contracts.LectureService;
import com.example.virtualteacher.utils.AssignmentMapper;
import com.example.virtualteacher.utils.HomeworkMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/assignments")
public class AssignmentController {

    private final AssignmentService assignmentService;
    private final LectureService lectureService;
    private final HomeworkService homeworkService;
    private final AssignmentMapper assignmentMapper;
    private final HomeworkMapper homeworkMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public AssignmentController(AssignmentService assignmentService,
                                LectureService lectureService,
                                HomeworkService homeworkService,
                                AssignmentMapper assignmentMapper,
                                HomeworkMapper homeworkMapper,
                                AuthenticationHelper authenticationHelper) {
        this.assignmentService = assignmentService;
        this.lectureService = lectureService;
        this.homeworkService = homeworkService;
        this.assignmentMapper = assignmentMapper;
        this.homeworkMapper = homeworkMapper;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping()
    public List<Assignment> getAll(@RequestHeader HttpHeaders headers) {
        try {
            return assignmentService.getAll(authenticationHelper.tryGetUser(headers));
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }


    @GetMapping("/{id}")
    public Assignment getById(@PathVariable int id) {
        return assignmentService.getById(id);
    }

    @GetMapping("/{id}/check")
    public List<Homework> checkHomeworks(@RequestHeader HttpHeaders headers,
                                         @PathVariable int id) {
        try {
            User teacher = authenticationHelper.tryGetUser(headers);
            return assignmentService.getHomeworks(id,teacher);
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }


    @PostMapping(consumes = {"multipart/form-data", "application/json"})
    public void createAssignment(@RequestHeader HttpHeaders headers,
                                 @RequestPart("AssignmentDto") AssignmentDto assignmentDto,
                                 @RequestParam("file") MultipartFile file) {

        try {
            assignmentDto.setTextFile(file);
            User creatingUser = authenticationHelper.tryGetUser(headers);
            Assignment assignment = assignmentMapper.dtoToAssignment(assignmentDto);
            assignmentService.create(assignment, creatingUser);
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @PutMapping("/{assignmentId}/attachment/{lectureId}")
    public Lecture attachAssignmentToLecture(@RequestHeader HttpHeaders headers, @PathVariable int assignmentId, @PathVariable int lectureId) {
        try {
            User attachingUser = authenticationHelper.tryGetUser(headers);
            assignmentService.attach(assignmentId, lectureId, attachingUser);
            return lectureService.getById(lectureId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @PutMapping("/{assignmentId}/detachment/{lectureId}")
    public Lecture detachLectureFromCourse(@RequestHeader HttpHeaders headers, @PathVariable int assignmentId, @PathVariable int lectureId) {
        try {
            User detachingUser = authenticationHelper.tryGetUser(headers);
            assignmentService.detach(assignmentId, lectureId, detachingUser);
            return lectureService.getById(lectureId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @PostMapping(path = "/{assignmentId}/homework", consumes = {"multipart/form-data", "application/json"})
    public void addHomework(@RequestHeader HttpHeaders headers,
                            @RequestPart("HomeworkDto") HomeworkDto homeworkDto,
                            @PathVariable int assignmentId,
                            @RequestParam("file") MultipartFile file) {
        homeworkDto.setTextFile(file);
        User studentSubmit = authenticationHelper.tryGetUser(headers);
        Homework homework = homeworkMapper.dtoToHomework(homeworkDto);
        homeworkService.create(homework, studentSubmit, assignmentService.getById(assignmentId));
    }


    @PutMapping(path = "/{assignmentId}/evaluation/{homeworkId}")
    public void evaluateHomework(@RequestHeader HttpHeaders headers,
                                 @PathVariable int assignmentId,
                                 @PathVariable int homeworkId,
                                 @RequestBody EvaluateHomeworkDto dto) {
        try {
            User teacher = authenticationHelper.tryGetUser(headers);
            homeworkService.evaluate(teacher, assignmentId, homeworkId, dto.getEvaluation());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }


}
