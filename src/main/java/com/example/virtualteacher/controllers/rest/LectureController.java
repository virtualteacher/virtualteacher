package com.example.virtualteacher.controllers.rest;

import com.example.virtualteacher.controllers.AuthenticationHelper;
import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.exceptions.InapplicableOperationException;
import com.example.virtualteacher.exceptions.UnauthorisedOperationException;
import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.Lecture;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.models.dtos.LectureDto;
import com.example.virtualteacher.services.contracts.CourseService;
import com.example.virtualteacher.services.contracts.LectureService;
import com.example.virtualteacher.utils.LectureMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/lectures")
public class LectureController {


    private final LectureService lectureService;
    private final CourseService courseService;
    private final LectureMapper mapper;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public LectureController(LectureService lectureService,
                             CourseService courseService,
                             LectureMapper mapper,
                             AuthenticationHelper authenticationHelper) {
        this.lectureService = lectureService;
        this.courseService = courseService;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Lecture> getAll() {
        return lectureService.getAll();
    }

    @GetMapping("/{id}")
    public Lecture getById(@PathVariable int id) {
        try {
            return lectureService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Lecture create(@RequestHeader HttpHeaders headers, @Valid @RequestBody LectureDto dto) {
        try {
            User creatingUser = authenticationHelper.tryGetUser(headers);
            var lecture = mapper.fromDto(dto);
            lectureService.create(lecture, creatingUser);
            return lecture;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnauthorisedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Lecture update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody LectureDto dto) {
        try {
            User updatingUser = authenticationHelper.tryGetUser(headers);
            var lectureToUpdate = mapper.fromDto(dto, id);
            lectureService.update(lectureToUpdate, updatingUser);
            return lectureToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }catch (UnauthorisedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @PutMapping("/{lectureId}/attachment/{courseId}")
    public Course attachLectureToCourse(@RequestHeader HttpHeaders headers,
                                        @PathVariable int lectureId,
                                        @PathVariable int courseId){
        try {
            lectureService.attach(lectureId,courseId,authenticationHelper.tryGetUser(headers));
           return courseService.getById(courseId);
        }  catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }catch (UnauthorisedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @PutMapping("/{lectureId}/detachment/{courseId}")
    public Course detachLectureFromCourse(@RequestHeader HttpHeaders headers,
                                          @PathVariable int lectureId,
                                          @PathVariable int courseId){
        try {
            lectureService.detach(lectureId,courseId,authenticationHelper.tryGetUser(headers));
            return courseService.getById(courseId);
        }  catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnauthorisedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User deletingUser = authenticationHelper.tryGetUser(headers);
            lectureService.delete(id, deletingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnauthorisedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }catch (InapplicableOperationException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }

}
