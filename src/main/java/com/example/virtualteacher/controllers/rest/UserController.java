package com.example.virtualteacher.controllers.rest;

import com.example.virtualteacher.controllers.AuthenticationHelper;
import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.exceptions.InapplicableOperationException;
import com.example.virtualteacher.exceptions.UnauthorisedOperationException;
import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.models.dtos.RegisterUserDto;
import com.example.virtualteacher.models.dtos.RoleDto;
import com.example.virtualteacher.models.dtos.UpdateUserDto;
import com.example.virtualteacher.services.contracts.UserService;
import com.example.virtualteacher.utils.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final UserMapper mapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService userService,
                          UserMapper mapper,
                          AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<User> getAll(@RequestHeader HttpHeaders headers) {
        try {
            User requestingUser = authenticationHelper.tryGetUser(headers);
            return userService.getAll(requestingUser);
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public User getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User requestingUser = authenticationHelper.tryGetUser(headers);
            return userService.getById(id, requestingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}/courses")
    public Set<Course> enrolledCourses(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User requestingUser = authenticationHelper.tryGetUser(headers);
            User user = userService.getById(id, requestingUser);
            return user.getCourses();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    public List<User> search(@RequestHeader HttpHeaders headers, @RequestParam String searchTerm) {
        try {
            User requestingUser = authenticationHelper.tryGetUser(headers);
            return userService.search(searchTerm, requestingUser);
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public User create(@Valid @RequestBody RegisterUserDto dto) {
        try {
            var user = mapper.fromDto(dto);
            if (!dto.getPassword().equals(dto.getPasswordConfirm())) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Password and passwordConfirm must be the same");
            }
            userService.create(user);
            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());

        }
    }

    @PutMapping("/{userForPromotionId}/promotion")

    public User promote(@RequestHeader HttpHeaders headers, @PathVariable int userForPromotionId, @Valid @RequestBody RoleDto dto) {
        try {
            User administrator = authenticationHelper.tryGetUser(headers);
            userService.addRole(userForPromotionId, administrator, dto.getRoleId());
            return userService.getById(userForPromotionId, administrator);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody UpdateUserDto dto) {
        try {
            User updatingUser = authenticationHelper.tryGetUser(headers);
            var userToUpdate = mapper.fromDto(dto, id);
            userService.update(userToUpdate, updatingUser);
            return userToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{userId}/enrollment/{courseId}")
    public void enrollToCourse(@RequestHeader HttpHeaders headers, @PathVariable int userId, @PathVariable int courseId) {
        try {
            User enrollingUser = authenticationHelper.tryGetUser(headers);
            var userToEnroll = userService.getById(userId, enrollingUser);
            userService.enroll(userToEnroll, courseId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (InapplicableOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User updatingUser = authenticationHelper.tryGetUser(headers);
            userService.delete(id, updatingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }catch (InapplicableOperationException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }
}