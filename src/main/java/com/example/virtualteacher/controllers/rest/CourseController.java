package com.example.virtualteacher.controllers.rest;

import com.example.virtualteacher.controllers.AuthenticationHelper;
import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.exceptions.InapplicableOperationException;
import com.example.virtualteacher.exceptions.UnauthorisedOperationException;
import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.models.dtos.CourseDto;
import com.example.virtualteacher.models.dtos.RatingDto;
import com.example.virtualteacher.services.contracts.CourseService;
import com.example.virtualteacher.services.contracts.RatingService;
import com.example.virtualteacher.utils.CourseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/courses")
public class CourseController {

    private final CourseService courseService;
    private final RatingService ratingService;
    private final CourseMapper mapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CourseController(CourseService courseService,
                            RatingService ratingService,
                            CourseMapper mapper,
                            AuthenticationHelper authenticationHelper) {
        this.courseService = courseService;
        this.ratingService = ratingService;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Course> getPublished() {
        return courseService.getPublished();
    }

    @GetMapping("/total")
    public List<Course> getAll(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        try {
            return courseService.getAll(user);
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Course getById(@PathVariable int id) {
        try {
            return courseService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<Course> filter(@RequestParam(required = false) String title,
                               @RequestParam(required = false) Integer topicId,
                               @RequestParam(required = false) Integer teacherId,
                               @RequestParam(required = false) Double rating) {

            return  courseService.filter(
                    Optional.ofNullable(title),
                    Optional.ofNullable(topicId),
                    Optional.ofNullable(teacherId),
                    Optional.ofNullable(rating));

    }

    @GetMapping("/sort")
    public List<Course> sort(@RequestParam(required = false)String title,
                             @RequestParam(required = false)String rating){
            return courseService.sort(Optional.ofNullable(title),
                                      Optional.ofNullable(rating));

    }

    @GetMapping("/search")
    public List<Course> search( @RequestParam String searchTerm) {
        return courseService.search(searchTerm);
    }


    @PutMapping("/{id}/publication")
    public void publish(@RequestHeader HttpHeaders headers,@PathVariable int id){
        try {
            courseService.publish(courseService.getById(id),authenticationHelper.tryGetUser(headers));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnauthorisedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @PostMapping
    public Course create(@RequestHeader HttpHeaders headers, @Valid @RequestBody CourseDto dto) {
        try {
            User creatingUser = authenticationHelper.tryGetUser(headers);
            var course = mapper.fromDto(dto);
            courseService.create(course, creatingUser);
            return course;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InapplicableOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }catch (UnauthorisedOperationException e){
            throw  new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @PutMapping("/{id}")
    public Course update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody CourseDto dto) {
        try {
            User updatingUser = authenticationHelper.tryGetUser(headers);
            var courseToUpdate = mapper.fromDto(dto, id);
            courseService.update(courseToUpdate, updatingUser);
            return courseToUpdate;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InapplicableOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }catch (UnauthorisedOperationException e){
            throw  new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User deletingUser = authenticationHelper.tryGetUser(headers);
            courseService.delete(id, deletingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnauthorisedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }catch (InapplicableOperationException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }



    @PostMapping("/{id}/rating")
    public void rate(@RequestHeader HttpHeaders headers, @PathVariable int id, @RequestBody RatingDto dto) {
        try {
            Course course = getById(id);
            User ratingUser = authenticationHelper.tryGetUser(headers);
            ratingService.rate(course, ratingUser, dto.getRating());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnauthorisedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

}
