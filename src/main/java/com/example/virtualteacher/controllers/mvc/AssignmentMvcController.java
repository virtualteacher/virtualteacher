package com.example.virtualteacher.controllers.mvc;


import com.example.virtualteacher.controllers.AuthenticationHelper;
import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.models.*;
import com.example.virtualteacher.models.dtos.AssignmentDto;
import com.example.virtualteacher.models.dtos.HomeworkDto;
import com.example.virtualteacher.services.contracts.AssignmentService;
import com.example.virtualteacher.services.contracts.HomeworkService;
import com.example.virtualteacher.services.contracts.LectureService;
import com.example.virtualteacher.utils.AssignmentMapper;
import com.example.virtualteacher.utils.HomeworkMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/assignments")
public class AssignmentMvcController {

    private final AssignmentService assignmentService;
    private final AssignmentMapper assignmentMapper;
    private final AuthenticationHelper authenticationHelper;
    private final HomeworkService homeworkService;
    private final HomeworkMapper homeworkMapper;
    private final LectureService lectureService;


    @Autowired
    public AssignmentMvcController(AssignmentService assignmentService,
                                   AssignmentMapper assignmentMapper,
                                   AuthenticationHelper authenticationHelper,
                                   HomeworkService homeworkService,
                                   HomeworkMapper homeworkMapper,
                                   LectureService lectureService) {
        this.assignmentService = assignmentService;
        this.assignmentMapper = assignmentMapper;
        this.authenticationHelper = authenticationHelper;
        this.homeworkService = homeworkService;
        this.homeworkMapper = homeworkMapper;
        this.lectureService = lectureService;

    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAuthorised")
    public boolean populateIsAuthorised(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User authorised = authenticationHelper.tryGetUser(session);
            return authorised.getRoles().stream().map(Role::getId).anyMatch(roleId -> roleId == 1 || roleId == 2);
        }
        return false;
    }

    @ModelAttribute("isStudent")
    public boolean populateIsStudent(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User authorised = authenticationHelper.tryGetUser(session);
            return authorised.getRoles().stream().map(Role::getId).anyMatch(roleId -> roleId == 3);
        }
        return false;
    }

    @ModelAttribute("lectures")
    public List<Lecture> populateLectures() {
        return lectureService.getAll();
    }

    @ModelAttribute("logged")
    public User populateUser(HttpSession session) {
        return authenticationHelper.tryGetUser(session);
    }

    @GetMapping
    public String showAllAssignments(HttpSession session, Model model) {
        model.addAttribute("assignments", assignmentService.getAll(authenticationHelper.tryGetUser(session)));
        return "assignments";

    }

    @GetMapping("/{id}")
    public String showSingleAssignment(@PathVariable int id, Model model) {
        try {
            Assignment assignment = assignmentService.getById(id);
            model.addAttribute("assignment", assignment);
            model.addAttribute("homeworks", assignment.getHomeworks());
            return "assignment";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/homework/{homeworkId}")
    public String showSingleHomework(@PathVariable int id,
                                     @PathVariable int homeworkId,
                                     Model model) {
        try {
            Assignment assignment = assignmentService.getById(id);
            Homework homework = assignment.getHomeworks().stream()
                    .filter(homework1 -> homework1.getId() == homeworkId).collect(Collectors.toList()).get(0);
            model.addAttribute("assignment", assignment);
            model.addAttribute("homework", homework);
            return "homework";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }




    @GetMapping("{id}/homework/new")
    public String showNewHomework(@PathVariable int id, Model model) {
        Assignment assignment = assignmentService.getById(id);
        model.addAttribute("assignment", assignment);
        model.addAttribute("homework", new HomeworkDto());
        return "homework-new";
    }

    @PostMapping("{assignmentId}/homework/new")
    public String createHomework(@PathVariable int assignmentId,
                                 @Valid @ModelAttribute("homework") HomeworkDto homeworkDto,
                                 BindingResult errors,
                                 HttpSession httpSession,
                                 Model model) {
        if (errors.hasErrors()) {
            return "homework-new";
        }
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            Assignment assignment = assignmentService.getById(assignmentId);
            Homework homework = homeworkMapper.dtoToHomework(homeworkDto);
            homeworkService.create(homework, user, assignment);
            return "redirect:/assignments/" + assignment.getId();

        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "bad-request";
        }

    }


    @GetMapping("/new")
    public String showNewAssignment(Model model) {
        model.addAttribute("assignment", new AssignmentDto());
        return "assignment-new";
    }

    @PostMapping("/new")
    public String createAssignment(@Valid @ModelAttribute("assignment") AssignmentDto assignmentDto,
                                   BindingResult errors,
                                   HttpSession httpSession,
                                   Model model) {
        if (errors.hasErrors()) {
            return "assignment-new";
        }
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            Assignment assignment = assignmentMapper.dtoToAssignment(assignmentDto);
            assignmentService.create(assignment, user);
            return "redirect:/assignments";
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "bad-request";
        }

    }

    @PostMapping("/{assignmentId}/attachment")
    public String attachAssignmentToLecture(HttpSession session,
                                            @PathVariable int assignmentId,
                                            @RequestParam int lectureId,
                                            Model model) {
        try {
            Assignment assignment = assignmentService.getById(assignmentId);
            model.addAttribute("assignmentId", assignment);
            assignmentService.attach(assignmentId, lectureId, authenticationHelper.tryGetUser(session));
            return "redirect:/assignments";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }


    }

    @PostMapping("/{assignmentId}/detachment")
    public String detachAssignmentFromLecture(HttpSession session,
                                              @PathVariable int assignmentId,
                                              @RequestParam int lectureId,
                                              Model model) {
        try {
            Assignment assignment = assignmentService.getById(assignmentId);
            model.addAttribute("assignmentId", assignment);
            assignmentService.detach(assignmentId, lectureId, authenticationHelper.tryGetUser(session));
            return "redirect:/assignments";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }

    }

    @PostMapping(path = "/{assignmentId}/homework/{homeworkId}/evaluation")
    public String evaluateHomework(HttpSession session,
                                   @PathVariable int assignmentId,
                                   @PathVariable int homeworkId,
                                   @RequestParam double evaluation) {
            User teacher = authenticationHelper.tryGetUser(session);
            homeworkService.evaluate(teacher, assignmentId, homeworkId, evaluation);
            return "redirect:/assignments/"+assignmentId+"/homework/"+homeworkId;

    }



}

