package com.example.virtualteacher.controllers.mvc;

import com.example.virtualteacher.controllers.AuthenticationHelper;
import com.example.virtualteacher.exceptions.*;
import com.example.virtualteacher.models.Role;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.models.dtos.PhotoDto;
import com.example.virtualteacher.models.dtos.RegisterUserDto;
import com.example.virtualteacher.models.dtos.UpdateUserDto;
import com.example.virtualteacher.services.contracts.RoleService;
import com.example.virtualteacher.services.contracts.UserService;
import com.example.virtualteacher.utils.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private static final User NOT_LOGGED_USER = new User();

    private final UserService userService;
    private final RoleService roleService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper mapper;

    @Autowired
    public UserMvcController(UserService userService,
                             UserMapper mapper,
                             RoleService roleService,
                             AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.mapper = mapper;
        this.roleService = roleService;
        this.authenticationHelper = authenticationHelper;
    }


    @ModelAttribute("roles")
    public List<Role> populateRoles() {
        return roleService.getAll()
                .stream()
                .filter(role -> role.getId() == 2 || role.getId() == 3).collect(Collectors.toList());
    }

    @ModelAttribute("forPromotion")
    public List<Role> populateForPromotion() {
        return roleService.getAll()
                .stream()
                .filter(role -> role.getId() == 1 || role.getId() == 2).collect(Collectors.toList());
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAuthorised")
    public boolean populateIsAuthorised(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User authorised = authenticationHelper.tryGetUser(session);
            return authorised.getRoles().stream().map(Role::getId).anyMatch(roleId -> roleId == 1 || roleId == 2);
        }
        return false;
    }

    @ModelAttribute("isAdministrator")
    public boolean populateIsAdministrator(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User admin = authenticationHelper.tryGetUser(session);
            return admin.getRoles().stream().map(Role::getId).anyMatch(roleId -> roleId == 1);
        }
        return false;
    }

    @ModelAttribute("isStudent")
    public boolean populateIsStudent(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User authorised = authenticationHelper.tryGetUser(session);
            return authorised.getRoles().stream().map(Role::getId).anyMatch(roleId -> roleId == 3);
        }
        return false;
    }

    @ModelAttribute("logged")
    public User populateUser(HttpSession session) {
        try {
            return authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return NOT_LOGGED_USER;
        }
    }

    @GetMapping
    public String showAllUsers(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        model.addAttribute("users", userService.getAll(user));
        return "users";
    }


    @GetMapping("/{id}")
    public String showSingleUser(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            User toVisualise = userService.getById(id, user);
            model.addAttribute("user", toVisualise);
            return "user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/search")
    public String search(@RequestParam String search, Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        model.addAttribute("search", userService.search(search, user));
        return "search-users";
    }


    @PostMapping("/enrollment/{courseId}")
    public String enrollToCourse(HttpSession session, @PathVariable int courseId, Model model) {
        try {
            User enrollingUser = authenticationHelper.tryGetUser(session);
            var userToEnroll = userService.getById(enrollingUser.getId(), enrollingUser);
            userService.enroll(userToEnroll, courseId);
            model.addAttribute("user", userToEnroll);
            return "redirect:/courses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (InapplicableOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "bad-request";
        }
    }

    @GetMapping("/courses")
    public String enrolledCourses(Model model, HttpSession session) {
        User requestingUser = authenticationHelper.tryGetUser(session);
        model.addAttribute("mycourses", requestingUser.getCourses());
        return "my-courses";
    }


    @GetMapping("/new")
    public String showNewUserPage(Model model) {
        model.addAttribute("user", new RegisterUserDto());
        return "user-new";
    }

    @PostMapping("/new")
    public String createUser(@Valid @ModelAttribute("user") RegisterUserDto userDto,
                             BindingResult errors) {
        if (errors.hasErrors()) {
            return "user-new";
        }

        if (!userDto.getPassword().equals(userDto.getPasswordConfirm())) {
            errors.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return "user-new";
        }

        try {
            User user = mapper.fromDto(userDto);
            userService.create(user);
            return "registration-success";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("email", "duplicate_email", e.getMessage());
            return "user-new";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            UpdateUserDto userDto = mapper.toDto(userService.getById(id, user));
            model.addAttribute("userId", id);
            model.addAttribute("user", userDto);
            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("user") UpdateUserDto dto,
                             BindingResult errors,
                             Model model,
                             HttpSession httpSession) {
        if (errors.hasErrors()) {
            return "user-update";
        }

        if (!dto.getPassword().equals(dto.getPasswordConfirm())) {
            errors.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return "user-update";
        }

        try {
            User updatingUser = authenticationHelper.tryGetUser(httpSession);
            User userToUpdate = mapper.fromDto(dto, id);
            userService.update(userToUpdate, updatingUser);
            return "redirect:/users/" + updatingUser.getId();
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/upload/photo")
    public String showEditProfilePhoto(Model model) {
        model.addAttribute("profilePicture", new PhotoDto());
        return "change-profile-picture";

    }

    @PostMapping("/upload/photo")
    public String editProfilePhoto(HttpSession session,
                                   @Valid @ModelAttribute("profilePicture") PhotoDto dto) {
        User user = authenticationHelper.tryGetUser(session);
        user = mapper.fromDto(dto, user.getId());
        userService.update(user, user);
        return "redirect:/users/" + user.getId();

    }


    @PostMapping("/{userForPromotionId}/promotion")
    public String promote(HttpSession httpSession,
                          @PathVariable int userForPromotionId,
                          @RequestParam int roleId,
                          Model model) {
        try {
            User administrator = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("roleId", roleId);
            userService.addRole(userForPromotionId, administrator, roleId);
            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorisedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession httpSession) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            userService.delete(id, user);
            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (InapplicableOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "delete-denied";
        }
    }

}
