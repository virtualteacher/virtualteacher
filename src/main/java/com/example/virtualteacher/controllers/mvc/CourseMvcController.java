package com.example.virtualteacher.controllers.mvc;

import com.example.virtualteacher.controllers.AuthenticationHelper;
import com.example.virtualteacher.exceptions.AuthenticationFailureException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.exceptions.InapplicableOperationException;
import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.Role;
import com.example.virtualteacher.models.Topic;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.models.dtos.CourseDto;
import com.example.virtualteacher.models.dtos.RatingDto;
import com.example.virtualteacher.services.contracts.CourseService;
import com.example.virtualteacher.services.contracts.RatingService;
import com.example.virtualteacher.services.contracts.TopicService;
import com.example.virtualteacher.utils.CourseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/courses")
public class CourseMvcController {


    private static final User NOT_LOGGED_USER = new User();

    private final CourseService courseService;
    private final CourseMapper mapper;
    private final AuthenticationHelper authenticationHelper;
    private final RatingService ratingService;
    private final TopicService topicService;


    @Autowired
    public CourseMvcController(CourseService courseService,
                               CourseMapper mapper,
                               AuthenticationHelper authenticationHelper,
                               TopicService topicService,
                               RatingService ratingService) {
        this.courseService = courseService;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;
        this.topicService = topicService;
        this.ratingService = ratingService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAuthorised")
    public boolean populateIsAuthorised(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User authorised = authenticationHelper.tryGetUser(session);
            return authorised.getRoles().stream().map(Role::getId).anyMatch(roleId -> roleId == 1 || roleId == 2);
        }
        return false;
    }

    @ModelAttribute("isStudent")
    public boolean populateIsStudent(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User authorised = authenticationHelper.tryGetUser(session);
            return authorised.getRoles().stream().map(Role::getId).anyMatch(roleId -> roleId == 3);
        }
        return false;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User authorised = authenticationHelper.tryGetUser(session);
            return authorised.getRoles().stream().map(Role::getId).anyMatch(roleId -> roleId == 1);
        }
        return false;
    }


    @ModelAttribute("topics")
    public List<Topic> populateTopics() {
        return topicService.getAll();
    }

    @ModelAttribute("currentDate")
    public LocalDate currentDate(){
        return LocalDate.now();
    }

    @ModelAttribute("teachers")
    public List<User> populateTeachers() {
     return courseService.populateTeachers();
    }

    @ModelAttribute("logged")
    public User populateUser(HttpSession session) {
        try {
            return authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return NOT_LOGGED_USER;
        }
    }

    @GetMapping
    public String showAllCourses(Model model) {
        model.addAttribute("published", courseService.getPublished());
        return "published-courses";

    }

    @GetMapping("/all")
    public String showAllCourses(Model model, HttpSession session) {
        model.addAttribute("all", courseService.getAll(authenticationHelper.tryGetUser(session)));
        return "all-courses";

    }


    @GetMapping("/filter")
    public String filter(@RequestParam(required = false) String title,
                         @RequestParam(required = false) Integer topicId,
                         @RequestParam(required = false) Integer teacherId,
                         @RequestParam(required = false) Double rating,
                         Model model) {
            model.addAttribute("filter", courseService.filter(
                    Optional.ofNullable(title),
                    Optional.ofNullable(topicId),
                    Optional.ofNullable(teacherId),
                    Optional.ofNullable(rating)));
            return "filter-courses";

    }

    @GetMapping("/sortByTitle")
    public String sortByTitle(Model model) {
        String title = "title";
        model.addAttribute("sortByTitle", courseService.sort(
                Optional.ofNullable(title),
                Optional.ofNullable(null)));
        return "sort-by-title";

    }

    @GetMapping("/sortByRating")
    public String sortByRating(Model model) {
        String rating = "rating";
        model.addAttribute("sortByRating", courseService.sort(
                Optional.ofNullable(null),
                Optional.ofNullable(rating)));
        return "sort-by-rating";

    }

    @GetMapping("/sortByTitleAndRating")
    public String sortByTitleAndRating(Model model) {
        String title = "title";
        String rating = "rating";
        model.addAttribute("sortByTitleAndRating", courseService.sort(
                Optional.ofNullable(title),
                Optional.ofNullable(rating)));
        return "sort-by-title-and-rating";

    }

    @GetMapping("/search")
    public String search(@RequestParam String search, Model model) {
        model.addAttribute("search", courseService.search(search));
        return "search-course";
    }


    @GetMapping("/all/searchAuthorised")
    public String searchAuthorised(@RequestParam String searchAuthorised, Model model) {
        model.addAttribute("searchAuthorised", courseService.searchAuthorised(searchAuthorised));
        return "search-authorised-course";
    }


    @GetMapping("/all/{id}")
    public String showSingleCourse(@PathVariable int id, Model model) {
        try {
            Course course = courseService.getById(id);
            model.addAttribute("lectures", course.getLectures());
            model.addAttribute("course", course);
            return "course";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }


    @GetMapping("/all/new")
    public String showNewParcelPage(Model model) {
        model.addAttribute("course", new CourseDto());
        return "course-new";
    }

    @PostMapping("/all/new")
    public String createCourse(@Valid @ModelAttribute("course") CourseDto courseDto,
                               BindingResult errors,
                               Model model,
                               HttpSession httpSession) {
        if (errors.hasErrors()) {
            return "course-new";
        }
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            Course course = mapper.fromDto(courseDto);
            courseService.create(course, user);
            return "redirect:/courses/all";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/all/{id}/publication")
    public String publish(HttpSession session, @PathVariable int id, Model model) {
        try {
            var course = courseService.getById(id);
            courseService.publish(course, authenticationHelper.tryGetUser(session));
            model.addAttribute("course", course);
            return "redirect:/courses/all";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("all/{id}/rating")
    public String rate(HttpSession session, @PathVariable int id, double rating, Model model) {
        try {
            Course course = courseService.getById(id);
            User ratingUser = authenticationHelper.tryGetUser(session);
            RatingDto dto = new RatingDto();
            dto.setRating(rating);

            ratingService.rate(course, ratingUser, dto.getRating());

            model.addAttribute("course", course);
            return "redirect:/users/courses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/all/{id}/update")
    public String showEditCoursePage(@PathVariable int id, Model model) {
        try {
            Course course = courseService.getById(id);
            CourseDto courseDto = mapper.toDto(course);
            model.addAttribute("courseId", id);
            model.addAttribute("course", courseDto);
            return "course-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/all/{id}/update")
    public String updateCourse(@PathVariable int id,
                               @Valid @ModelAttribute("course") CourseDto dto,
                               BindingResult errors,
                               Model model,
                               HttpSession httpSession) {
        if (errors.hasErrors()) {
            return "course-update";
        }
        try {
            User updatingUser = authenticationHelper.tryGetUser(httpSession);
            Course courseToUpdate = mapper.fromDto(dto, id);
            courseService.update(courseToUpdate, updatingUser);
            return "redirect:/courses/all";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }


    @GetMapping("/all/{id}/delete")
    public String deleteCourse(@PathVariable int id, Model model, HttpSession httpSession) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            courseService.delete(id, user);
            return "redirect:/all-courses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }catch (InapplicableOperationException e){
            model.addAttribute("error",e.getMessage());
            return "bad-request";
        }
    }


}
