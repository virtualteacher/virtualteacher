package com.example.virtualteacher.controllers.mvc;

import com.example.virtualteacher.controllers.AuthenticationHelper;
import com.example.virtualteacher.exceptions.AuthenticationFailureException;
import com.example.virtualteacher.models.Role;
import com.example.virtualteacher.models.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private static final User NOT_LOGGED_USER = new User();

    private final AuthenticationHelper authenticationHelper;

    public HomeMvcController(AuthenticationHelper authenticationHelper) {
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAuthorised")
    public boolean populateIsAuthorised(HttpSession session) {
     if (populateIsAuthenticated(session)) {
            User authorised = authenticationHelper.tryGetUser(session);
            return authorised.getRoles().stream().map(Role::getId).anyMatch(roleId -> roleId == 1 || roleId == 2);
        }
     return false;
    }

    @ModelAttribute("currentDate")
    public LocalDate currentDate(){
        return LocalDate.now();
    }

    @ModelAttribute("isStudent")
    public boolean populateIsStudent(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User authorised = authenticationHelper.tryGetUser(session);
            return authorised.getRoles().stream().map(Role::getId).anyMatch(roleId -> roleId == 3);
        }
        return false;
    }


    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User authorised = authenticationHelper.tryGetUser(session);
            return authorised.getRoles().stream().map(Role::getId).anyMatch(roleId -> roleId == 1);
        }
        return false;
    }


    @ModelAttribute("logged")
    public User populateUser(HttpSession session) {
        try {
            return authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return NOT_LOGGED_USER;
        }
    }

    @GetMapping
    public String showHomePage() {
        return "index";
    }
}