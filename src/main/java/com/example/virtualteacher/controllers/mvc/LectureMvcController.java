package com.example.virtualteacher.controllers.mvc;

import com.example.virtualteacher.controllers.AuthenticationHelper;
import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.exceptions.InapplicableOperationException;
import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.Lecture;
import com.example.virtualteacher.models.Role;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.models.dtos.LectureDto;
import com.example.virtualteacher.services.contracts.CourseService;
import com.example.virtualteacher.services.contracts.LectureService;
import com.example.virtualteacher.utils.LectureMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/lectures")
public class LectureMvcController {

    private final LectureService lectureService;
    private final LectureMapper lectureMapper;
    private final AuthenticationHelper authenticationHelper;
    private final CourseService courseService;


    @Autowired
    public LectureMvcController(LectureService lectureService,
                                LectureMapper lectureMapper,
                                CourseService courseService,
                                AuthenticationHelper authenticationHelper) {
        this.lectureService = lectureService;
        this.lectureMapper = lectureMapper;
        this.courseService = courseService;
        this.authenticationHelper = authenticationHelper;

    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAuthorised")
    public boolean populateIsAuthorised(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User authorised = authenticationHelper.tryGetUser(session);
            return authorised.getRoles().stream().map(Role::getId).anyMatch(roleId -> roleId == 1 || roleId == 2);
        }
        return false;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User authorised = authenticationHelper.tryGetUser(session);
            return authorised.getRoles().stream().map(Role::getId).anyMatch(roleId -> roleId == 1);
        }
        return false;
    }

    @ModelAttribute("courses")
    public List<Course> populateCourses() {
        return courseService.getPublished();
    }


   @ModelAttribute("allCourses")
   public List<Course> populateAllCourses(HttpSession session) {
        if(populateIsAdmin(session)) {
            return courseService.getAll(authenticationHelper.tryGetUser(session));
        }else {
             return populateCourses();
        }

   }

    @ModelAttribute("logged")
    public User populateUser(HttpSession session) {
        return authenticationHelper.tryGetUser(session);
    }

    @ModelAttribute("isStudent")
    public boolean populateIsStudent(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User authorised = authenticationHelper.tryGetUser(session);
            return authorised.getRoles().stream().map(Role::getId).anyMatch(roleId -> roleId == 3);
        }
        return false;
    }

    @GetMapping
    public String showAllLectures(Model model) {
        model.addAttribute("lectures", lectureService.getAll());
        return "lectures";

    }

    @GetMapping("/{id}")
    public String showSingleLecture(@PathVariable int id, Model model) {
        try {
            Lecture lecture = lectureService.getById(id);
            model.addAttribute("lecture", lecture);
            model.addAttribute("assignments", lecture.getAssignments());
            return "lecture";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showNewLecturePage(Model model) {
        model.addAttribute("lecture", new LectureDto());
        return "lecture-new";
    }

    @PostMapping("/new")
    public String createLecture(@Valid @ModelAttribute("lecture") LectureDto lectureDto,
                                BindingResult errors,
                                HttpSession httpSession,
                                Model model) {
        if (errors.hasErrors()) {
            return "lecture-new";
        }
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            Lecture lecture = lectureMapper.fromDto(lectureDto);
            lectureService.create(lecture, user);
            return "redirect:/lectures";
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "bad-request";
        }

    }

    @GetMapping("/{id}/update")
    public String showEditLecturePage(@PathVariable int id, Model model) {
        try {
            Lecture lecture = lectureService.getById(id);
            LectureDto lectureDto = lectureMapper.toDto(lecture);
            model.addAttribute("lectureId", id);
            model.addAttribute("lecture", lectureDto);
            return "lecture-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateLecture(@PathVariable int id,
                                @Valid @ModelAttribute("lecture") LectureDto dto,
                                BindingResult errors,
                                Model model,
                                HttpSession httpSession) {
        if (errors.hasErrors()) {
            return "lecture-update";
        }
        try {
            User updatingUser = authenticationHelper.tryGetUser(httpSession);
            Lecture lectureToUpdate = lectureMapper.fromDto(dto, id);
            lectureService.update(lectureToUpdate, updatingUser);
            return "redirect:/lectures";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "bad-request";
        }
    }

    @PostMapping("/{id}/attachment")
    public String attachLectureToCourse(HttpSession session,
                                        @PathVariable int id,
                                        @RequestParam int courseId,
                                        Model model) {
        try {
            model.addAttribute("courseId", courseId);
            lectureService.attach(id, courseId, authenticationHelper.tryGetUser(session));
            return "redirect:/lectures";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "bad-request";
        }
    }

    @PostMapping("/{id}/detachment")
    public String detachLectureFromCourse(HttpSession session,
                                          @PathVariable int id,
                                          @RequestParam int courseId,
                                          Model model) {
        try {
            model.addAttribute("courseId", courseId);
            lectureService.detach(id, courseId, authenticationHelper.tryGetUser(session));
            return "redirect:/lectures";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteLecture(@PathVariable int id, Model model, HttpSession httpSession) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            lectureService.delete(id, user);
            return "redirect:/lectures";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (InapplicableOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "bad-request";
        }
    }
}

