package com.example.virtualteacher.models;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
@Entity
@Table(name = "statuses")
public class Status {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "status_id")
    private int id;

    @NotBlank(message = "Status type can't be empty")
    @Size(min = 5, max = 10, message = "Status type should be between 5 and 10 symbols")
    @Column(name = "type")
    private String type;


    public Status() {
    }

    public Status( int id,String type) {
        setId(id);
        setType(type);

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}