package com.example.virtualteacher.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "courses")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "course_id")
    private int id;

    @Column(name = "title")
    private String title;

    @OneToOne
    @JoinColumn(name = "topic_id")
    private Topic topic;

    @Column(name = "description")
    private String description;

    @Column(name = "starting_date")
    private LocalDate startingDate;

    @Column(name = "rating")
    private Double rating;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "status_id")
    private Status status;


    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "courses_teachers",
            joinColumns = @JoinColumn(name = "course_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")

    )
    private Set<User> teachers;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "courses_lectures",
            joinColumns = @JoinColumn(name = "course_id"),
            inverseJoinColumns = @JoinColumn(name = "lecture_id")

    )
    private Set<Lecture> lectures;

    public Course(){

    }

    public Course(int id,
                  String title,
                  Topic topic,
                  String description,
                  LocalDate startingDate,
                  Status status) {
       setId(id);
       setTitle(title);
       setTopic(topic);
       setDescription(description);
       setStartingDate(startingDate);
       setStatus(status);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(LocalDate startingDate) {
        this.startingDate = startingDate;
    }

    public Set<User> getTeachers() {
        return teachers;
    }

    public void setTeachers(Set<User> teachers) {
        this.teachers = teachers;
    }

    public Set<Lecture> getLectures() {
        return lectures;
    }

    public void setLectures(Set<Lecture> lectures) {
        this.lectures = lectures;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Double getRating() {
        return rating;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Course course = (Course) obj;
        return getId() == course.getId() &&
                getTitle().equals(course.getTitle());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(),getTitle());
    }


}
