package com.example.virtualteacher.models.dtos;

import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Size;

public class LectureDto {

    @Size(min = 5, max = 50, message = "Title should be between 5 and 50 symbols!")
    private String title;

    @Size(min = 1, max = 1000, message = "Description should be between 1 and 1000 symbols!")
    private String description;

    @URL(message = "Please enter a valid link!")
    private String videoUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
}
