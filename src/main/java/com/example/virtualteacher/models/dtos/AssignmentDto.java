package com.example.virtualteacher.models.dtos;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;

public class AssignmentDto {
    @NotBlank(message = "This field cannot be empty")
    private String name;

    private MultipartFile textFile;

    public AssignmentDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MultipartFile getTextFile() {
        return textFile;
    }

    public void setTextFile(MultipartFile textFile) {
        this.textFile = textFile;
    }
}