package com.example.virtualteacher.models.dtos;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class RoleDto {

    @Min(1)
    @Max(3)
    private int roleId;

    public RoleDto() {
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
}
