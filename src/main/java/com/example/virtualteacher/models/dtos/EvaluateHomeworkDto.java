package com.example.virtualteacher.models.dtos;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class EvaluateHomeworkDto {

    @Min(2)
    @Max(6)
    private double evaluation;

    public EvaluateHomeworkDto() {
    }

    public double getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(double evaluation) {
        this.evaluation = evaluation;
    }
}
