package com.example.virtualteacher.models.dtos;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class RatingDto {


    @Min(1)
    @Max(10)
    private double rating;

    public RatingDto() {
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
