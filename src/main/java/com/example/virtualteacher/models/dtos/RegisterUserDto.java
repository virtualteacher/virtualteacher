package com.example.virtualteacher.models.dtos;

import javax.validation.constraints.*;

public class RegisterUserDto extends LoginDto {

    private static final String ROLE_VALIDATION_MESSAGE = "Please pick between 1 and 2!";
    @NotBlank(message = "First name can't be empty")
    @Size(min = 2, max = 20, message = "First name should be between 2 and 20 symbols!")
    private String firstName;

    @NotBlank(message = "Last name can't be empty")
    @Size(min = 2, max = 20, message = "Last name should be between 2 and 20 symbols!")
    private String lastName;

    private String passwordConfirm;

    @Min(value = 2,message = ROLE_VALIDATION_MESSAGE)
    @Max(value = 3,message = ROLE_VALIDATION_MESSAGE)
    private int roleId;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
}
