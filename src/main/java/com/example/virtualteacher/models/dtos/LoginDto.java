package com.example.virtualteacher.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class LoginDto {

    @NotBlank(message = "Email field cannot be blank!")
    @Email(message = "Invalid email address!")
    private String email;

    @NotBlank(message = "Password field cannot be empty!")
    @Size(min = 8, max = 20, message = "Password should be between 8 and 20 symbols!")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!?])(?=\\S+$).{8,}$",message = "Password must contain uppercase and lowercase letters, number and a special symbol!")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
