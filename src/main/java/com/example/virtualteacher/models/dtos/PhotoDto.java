package com.example.virtualteacher.models.dtos;

import org.springframework.web.multipart.MultipartFile;

public class PhotoDto {
    private MultipartFile profilePhoto;

    public void setProfilePhoto(MultipartFile profilePhoto) {
        if (!profilePhoto.getContentType().equalsIgnoreCase("image/jpeg") &&
                !profilePhoto.getContentType().equalsIgnoreCase("image/png") &&
                !profilePhoto.getContentType().equalsIgnoreCase("image/svg+xml") &&
                !profilePhoto.getContentType().equalsIgnoreCase("image/tiff")){
            throw new IllegalArgumentException("Acceptable file types: jpg,png,svg and tiff.");
        }
        this.profilePhoto = profilePhoto;
    }

    public MultipartFile getProfilePhoto() {
        return profilePhoto;
    }
}
