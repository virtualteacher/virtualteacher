package com.example.virtualteacher.models.dtos;


import javax.validation.constraints.*;

public class UpdateUserDto {

    @NotBlank(message = "First name can't be empty")
    @Size(min = 2, max = 15, message = "First name should be between 2 and 20 symbols")
    private String firstName;

    @NotBlank(message = "Last name can't be empty")
    @Size(min = 2, max = 15, message = "Last name should be between 2 and 20 symbols")
    private String lastName;


    @NotBlank(message = "Password field cannot be empty")
    @Size(min = 8, max = 20, message = "Password should be between 8 and 20 symbols!")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!?])(?=\\S+$).{8,}$",message = "Invalid password")
    private String password;

    @NotBlank(message = "Password confirm field cannot be empty")
    private String passwordConfirm;



    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
