package com.example.virtualteacher.models;

import javax.persistence.*;

@Entity
@Table(name = "homeworks")
public class Homework {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "homework_id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "file_path")
    private String filePath;

    @Column (name = "user_email")
    private String userEmail;

    @ManyToOne
    @JoinColumn (name = "assignment_id")
    private Assignment assignment;

    @Column(name = "evaluation")
    private double evaluation;

    public Homework() {
    }
    public Homework(String name,String filePath,String userEmail,Assignment assignment){
        setName(name);
        setFilePath(filePath);
        setUserEmail(userEmail);
        setAssignment(assignment);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public double getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(double evaluation) {
        this.evaluation = evaluation;
    }
}
