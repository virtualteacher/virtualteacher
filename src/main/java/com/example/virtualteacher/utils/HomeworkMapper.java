package com.example.virtualteacher.utils;

import com.example.virtualteacher.models.Homework;
import com.example.virtualteacher.models.dtos.HomeworkDto;
import com.example.virtualteacher.services.contracts.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HomeworkMapper {

    private final FileService fileService;

    @Autowired
    public HomeworkMapper(FileService fileService) {
        this.fileService = fileService;
    }

    public Homework dtoToHomework(HomeworkDto homeworkDto) {
        Homework homework = new Homework();
        homework.setName(homeworkDto.getName());
        homework.setFilePath(fileService.saveFile(homeworkDto.getTextFile(),homework.getName()));

        return homework;
    }
}