package com.example.virtualteacher.utils;

import com.example.virtualteacher.models.Assignment;
import com.example.virtualteacher.models.dtos.AssignmentDto;
import com.example.virtualteacher.services.contracts.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AssignmentMapper {

    private final FileService fileService;

    @Autowired
    public AssignmentMapper(FileService fileService) {
        this.fileService = fileService;
    }

    public Assignment dtoToAssignment(AssignmentDto assignmentDto) {
        Assignment assignment = new Assignment();
        assignment.setName(assignmentDto.getName());
        assignment.setFilePath(fileService.saveFile(assignmentDto.getTextFile(),assignment.getName()));

        return assignment;
    }
}
