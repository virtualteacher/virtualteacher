package com.example.virtualteacher.utils;

import com.example.virtualteacher.exceptions.InapplicableOperationException;
import com.example.virtualteacher.exceptions.UnauthorisedOperationException;
import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.Role;
import com.example.virtualteacher.models.User;

public class AuthorisationChecker {

    private static final String UNAUTHORISED_USER = "Only teacher or administrator is authorised for this operation!";
    private static final String ONLY_ADMIN = "Only administrators are authorised for this operation!";
    private static final String NOT_A_TEACHER = "Only teachers can be assigned to a course!";
    private static final String NOT_ENROLLED = "Only a student enrolled for a current course can rate it!";
    private static final String NOT_A_STUDENT = "Only students can enroll for a course!";
    private static final String NOT_OWNER_TEACHER_OR_ADMINISTRATOR = "Only administrator,teacher or the owner of the profile is authorised for this operation!";

    public static void authorise(User user) {
        boolean UserIsNotTeacherOrAdmin = user.getRoles().stream().map(Role::getId).noneMatch(roleId -> roleId == 1 || roleId == 2);
        if (UserIsNotTeacherOrAdmin) {
            throw new UnauthorisedOperationException(UNAUTHORISED_USER);
        }
    }

    public static void authoriseAdmin(User user) {
        boolean UserIsNotAdmin = user.getRoles().stream().map(Role::getId).noneMatch(roleId -> roleId == 1);
        if (UserIsNotAdmin) {
            throw new UnauthorisedOperationException(ONLY_ADMIN);
        }
    }

    public static void checkIfTeacher(User user) {
        boolean userIsNotTeacher = user.getRoles().stream().map(Role::getId).noneMatch(roleId -> roleId == 2);
        if (userIsNotTeacher) {
            throw new InapplicableOperationException(NOT_A_TEACHER);
        }
    }

    public static void checkIfStudentEnrolledForCourse(User user, Course course) {
        boolean StudentNotEnrolled = user.getCourses().stream().noneMatch(c -> c.getTitle().equals(course.getTitle()));
        if (StudentNotEnrolled) {
            throw new UnauthorisedOperationException(NOT_ENROLLED);
        }
    }

    public static void checkIfStudent(User user) {
        boolean userIsNotStudent = user.getRoles().stream().map(Role::getId).noneMatch(roleId -> roleId == 3);
        if (userIsNotStudent) {
            throw new InapplicableOperationException(NOT_A_STUDENT);
        }
    }

    public static void authorisedOrOwner(User user, int id) {
        boolean userNotOwner = user.getId() != id;
        boolean UserIsNotTeacherOrAdmin = user.getRoles().stream().map(Role::getId).noneMatch(roleId -> roleId == 1 || roleId == 2);
        if (userNotOwner&&UserIsNotTeacherOrAdmin) {
            throw new UnauthorisedOperationException(NOT_OWNER_TEACHER_OR_ADMINISTRATOR);
        }
    }
}


