package com.example.virtualteacher.utils;

import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.Lecture;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.models.dtos.CourseDto;
import com.example.virtualteacher.repository.contracts.UserRepository;
import com.example.virtualteacher.services.contracts.CourseService;
import com.example.virtualteacher.services.contracts.StatusService;
import com.example.virtualteacher.services.contracts.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class CourseMapper {

    private final CourseService courseService;
    private final TopicService topicService;
    private final StatusService statusService;
    private final UserRepository userRepository;

    @Autowired
    CourseMapper(CourseService courseService,
                 TopicService topicService,
                 StatusService statusService,
                 UserRepository userRepository) {
        this.courseService = courseService;
        this.topicService = topicService;
        this.statusService = statusService;
        this.userRepository = userRepository;
    }

    public CourseDto toDto(Course course) {
        CourseDto courseDto = new CourseDto();
        courseDto.setTitle(course.getTitle());
        courseDto.setDescription(course.getDescription());
        courseDto.setTopicId(course.getTopic().getId());
        courseDto.setStartingDate(course.getStartingDate());

        return courseDto;
    }

    public Course fromDto(CourseDto dto) {
        Course courseToCreate =  new Course();
        Set<Lecture> lectures = new HashSet<>();
        courseToCreate.setLectures(lectures);
        Set<User> teachers = new HashSet<>();
        courseToCreate.setTeachers(teachers);
        fromDtoToObject(dto, courseToCreate);
        return courseToCreate;
    }

    public Course fromDto(CourseDto dto, int id){
        Course courseToUpdate = courseService.getById(id);
        fromDtoToObject(dto, courseToUpdate);
        return courseToUpdate;
    }


    private void fromDtoToObject(CourseDto dto, Course course) {
       course.setTitle(dto.getTitle());
       course.setDescription(dto.getDescription());
       course.setTopic(topicService.getById(dto.getTopicId()));
       course.setStartingDate(dto.getStartingDate());
        if(course.getTeachers().stream().noneMatch(teacher -> teacher.getId()== dto.getTeacherId())){
            course.getTeachers().add(userRepository.getById(dto.getTeacherId()));
        }
        course.setStatus(statusService.getById(1));
        course.setRating(0.0);

    }

}
