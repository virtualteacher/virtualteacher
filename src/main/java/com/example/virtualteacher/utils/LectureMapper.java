package com.example.virtualteacher.utils;

import com.example.virtualteacher.models.Lecture;
import com.example.virtualteacher.models.dtos.LectureDto;
import com.example.virtualteacher.services.contracts.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LectureMapper {

    private final LectureService lectureService;

    @Autowired
    LectureMapper(LectureService lectureService) {
        this.lectureService = lectureService;
    }

    public LectureDto toDto(Lecture lecture) {
        LectureDto lectureDto = new LectureDto();
        lectureDto.setTitle(lecture.getTitle());
        lectureDto.setDescription(lecture.getDescription());
        lectureDto.setVideoUrl(lecture.getVideoUrl());

        return lectureDto;
    }

    public Lecture fromDto(LectureDto dto) {
        Lecture lectureToCreate =  new Lecture();
        fromDtoToObject(dto, lectureToCreate);
        return lectureToCreate;
    }

    public Lecture fromDto(LectureDto dto, int id){
        Lecture lectureToUpdate = lectureService.getById(id);
        fromDtoToObject(dto, lectureToUpdate);
        return lectureToUpdate;
    }

    private void fromDtoToObject(LectureDto dto, Lecture lecture) {
        lecture.setTitle(dto.getTitle());
        lecture.setDescription(dto.getDescription());
        lecture.setVideoUrl(dto.getVideoUrl());
    }

}
