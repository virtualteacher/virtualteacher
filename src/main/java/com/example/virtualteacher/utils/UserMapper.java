package com.example.virtualteacher.utils;

import com.example.virtualteacher.models.Role;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.models.dtos.PhotoDto;
import com.example.virtualteacher.models.dtos.RegisterUserDto;
import com.example.virtualteacher.models.dtos.UpdateUserDto;
import com.example.virtualteacher.repository.contracts.UserRepository;
import com.example.virtualteacher.services.contracts.FileService;
import com.example.virtualteacher.services.contracts.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class UserMapper {

    private final UserRepository userRepository;
    private final RoleService roleService;
    private final FileService fileService;

    @Autowired
    public UserMapper(UserRepository userRepository,
                      RoleService roleService,
                      FileService fileService){
        this.userRepository = userRepository;
        this.roleService = roleService;
        this.fileService = fileService;

    }

    public UpdateUserDto toDto(User user) {
        UpdateUserDto updateUserDto = new UpdateUserDto();
        updateUserDto.setFirstName(user.getFirstName());
        updateUserDto.setLastName(user.getLastName());
       updateUserDto.setPassword(user.getPassword());

        return updateUserDto;
    }



    public User fromDto(RegisterUserDto dto) {
        User userToCreate = new User();
        fromDtoToObject(dto, userToCreate);
        return userToCreate;
    }

    public User fromDto(UpdateUserDto dto, int id){
        User userToUpdate = userRepository.getById(id);
        fromDtoToObject(dto, userToUpdate);
        return userToUpdate;
    }

    public User fromDto(PhotoDto dto, int id){
        User userToUpdate = userRepository.getById(id);
        fromDtoToObject(dto, userToUpdate);
        return userToUpdate;
    }

    private void fromDtoToObject(RegisterUserDto dto, User user){
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setEmail(dto.getEmail());
        user.setPassword(dto.getPasswordConfirm());
        Set<Role> roles = new HashSet<>();
        roles.add(roleService.getById(dto.getRoleId()));
        user.setRoles(roles);
        user.setProfilePicture("../assets/img/no-avatar.png");
    }

    private void fromDtoToObject(UpdateUserDto dto, User user){
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setPassword(dto.getPasswordConfirm());

    }

    private void fromDtoToObject(PhotoDto dto, User user){
        user.setProfilePicture(fileService.saveFile(dto.getProfilePhoto(),"photo"));

    }
}
