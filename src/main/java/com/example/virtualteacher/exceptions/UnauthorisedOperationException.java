package com.example.virtualteacher.exceptions;

public class UnauthorisedOperationException extends RuntimeException {
    public UnauthorisedOperationException(String message){
        super(String.format("Access denied! %s",message));
    }
}

