package com.example.virtualteacher.exceptions;

public class InapplicableOperationException extends RuntimeException{
    public InapplicableOperationException(String message){
        super(String.format("Attempt denied! %s",message));
    }
}
