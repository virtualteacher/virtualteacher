package com.example.virtualteacher.services.contracts;


import com.example.virtualteacher.models.Lecture;
import com.example.virtualteacher.models.User;

import java.util.List;

public interface LectureService {

    List<Lecture> getAll();

    Lecture getById(int id);

    void create(Lecture lecture, User creatingUser);

    void update(Lecture lecture, User updatingUser);

    void attach(int lectureId,int courseId,User user);

    void detach (int lectureId, int courseId,User user);

    void delete(int id, User deletingUser);
}
