package com.example.virtualteacher.services.contracts;

import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.User;

import java.util.List;
import java.util.Optional;

public interface CourseService {

    List<Course> getPublished();

    List<Course> getAll(User user);

    Course getById(int id);

    List<User> populateTeachers();

    List<Course> search(String searchTerm);

    List<Course> searchAuthorised(String searchTerm);

    List<Course> filter(Optional<String> title,
                        Optional<Integer> topicId,
                        Optional<Integer> teacherId,
                        Optional<Double> rating);

    List<Course> sort(Optional<String> title, Optional<String> rating);

    void publish(Course course, User publisher);

    void create(Course course, User creatingUser);

    void update(Course course, User updatingUser);

    void delete(int id, User deletingUser);

}
