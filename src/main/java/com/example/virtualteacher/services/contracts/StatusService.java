package com.example.virtualteacher.services.contracts;

import com.example.virtualteacher.models.Status;

import java.util.List;

public interface StatusService {

    List<Status> getAll();

    Status getById(int id);
}
