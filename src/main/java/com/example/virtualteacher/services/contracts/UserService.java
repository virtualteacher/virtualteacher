package com.example.virtualteacher.services.contracts;

import com.example.virtualteacher.models.User;

import java.util.List;

public interface UserService {

    List<User> getAll(User requestingUser);

    User getById(int id, User requestingUser);

    User getByEmail(String email);

    List<User> search(String searchTerm, User requestingUser);

    void enroll(User user, int courseId);

    void create(User user);

    void addRole(int userId,User admin,int roleId);

    void update(User user, User updatingUser);

    void delete(int id, User updatingUser);
}
