package com.example.virtualteacher.services.contracts;

import com.example.virtualteacher.models.Assignment;
import com.example.virtualteacher.models.Homework;
import com.example.virtualteacher.models.User;

import java.util.List;

public interface HomeworkService {

    List<Homework> getAll();

    Homework getById(int id);

    void create(Homework homework, User creatingUser,Assignment assignment);

    void evaluate(User teacher,int assignmentId,int homeworkId,double evaluation);
}
