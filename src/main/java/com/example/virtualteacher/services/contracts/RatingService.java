package com.example.virtualteacher.services.contracts;


import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.User;

public interface RatingService {
    void rate(Course course, User user, double rating);

    Double getAverageRating(Course course);
}
