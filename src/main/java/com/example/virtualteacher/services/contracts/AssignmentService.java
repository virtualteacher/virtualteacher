package com.example.virtualteacher.services.contracts;

import com.example.virtualteacher.models.Assignment;
import com.example.virtualteacher.models.Homework;
import com.example.virtualteacher.models.User;

import java.util.List;

public interface AssignmentService {

    List<Assignment> getAll(User user);

    Assignment getById(int id);

    List<Homework>  getHomeworks(int assignmentId,User teacher);

    void create(Assignment assignment, User creatingUser);

    void attach(int assignmentId, int lectureId, User attachingUser);

    void detach(int assignmentId, int lectureId, User detachingUser);

}
