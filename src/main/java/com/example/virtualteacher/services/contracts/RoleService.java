package com.example.virtualteacher.services.contracts;

import com.example.virtualteacher.models.Role;

import java.util.List;

public interface RoleService {

    List<Role> getAll();

    Role getById(int id);

}
