package com.example.virtualteacher.services.contracts;


import org.springframework.core.io.FileSystemResource;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {

    String saveFile(MultipartFile file, String title);

    FileSystemResource getFile(String fileName);
}