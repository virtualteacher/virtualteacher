package com.example.virtualteacher.services.contracts;

import com.example.virtualteacher.models.Topic;

import java.util.List;

public interface TopicService {

    List<Topic> getAll();

     Topic getById(int id);
}
