package com.example.virtualteacher.services;

import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.models.*;
import com.example.virtualteacher.repository.contracts.AssignmentRepository;
import com.example.virtualteacher.repository.contracts.LectureRepository;
import com.example.virtualteacher.services.contracts.AssignmentService;
import com.example.virtualteacher.utils.AuthorisationChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssignmentServiceImpl implements AssignmentService {

    private final AssignmentRepository assignmentRepository;
    private final LectureRepository lectureRepository;

    @Autowired
    public AssignmentServiceImpl(AssignmentRepository assignmentRepository,
                                 LectureRepository lectureRepository) {
        this.assignmentRepository = assignmentRepository;
        this.lectureRepository = lectureRepository;
    }

    @Override
    public List<Assignment> getAll(User user) {
        AuthorisationChecker.authorise(user);
        return assignmentRepository.getAll();
    }

    @Override
    public Assignment getById(int id) {
        return assignmentRepository.getById(id);
    }

    @Override
    public List<Homework> getHomeworks(int assignmentId, User teacher) {
        AuthorisationChecker.authorise(teacher);
        return assignmentRepository.getById(assignmentId).getHomeworks();
    }


    @Override
    public void create(Assignment assignment, User creatingUser) {
       AuthorisationChecker.authorise(creatingUser);
        boolean duplicateExists = true;
        try {
            assignmentRepository.getByField("name", assignment.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Assignment", "name", assignment.getName());
        }
        assignmentRepository.create(assignment);
    }

    @Override
    public void attach(int assignmentId, int lectureId, User attachingUser) {
        AuthorisationChecker.authorise(attachingUser);
        Lecture lecture = lectureRepository.getById(lectureId);
        if (lecture.getAssignments().stream().anyMatch(a -> a.getId() == assignmentId)) {
            throw new DuplicateEntityException("Assignment", "name", assignmentRepository.getById(assignmentId).getName());
        }
        lecture.getAssignments().add(assignmentRepository.getById(assignmentId));
        lectureRepository.update(lecture);

    }

    @Override
    public void detach(int assignmentId, int lectureId, User detachingUser) {
        AuthorisationChecker.authorise(detachingUser);
        Lecture lecture = lectureRepository.getById(lectureId);
        if (lecture.getAssignments().stream().noneMatch(a -> a.getId() == assignmentId)) {
            throw new EntityNotFoundException("Assignment", "name", assignmentRepository.getById(assignmentId).getName());
        }
        Assignment assignment = lecture.getAssignments().stream().filter(a1 -> a1.getId() == assignmentId).findAny().get();
        lecture.getAssignments().remove(assignment);
        lectureRepository.update(lecture);

    }


}
