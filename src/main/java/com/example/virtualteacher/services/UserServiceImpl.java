package com.example.virtualteacher.services;

import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.exceptions.InapplicableOperationException;
import com.example.virtualteacher.exceptions.UnauthorisedOperationException;
import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.repository.contracts.CourseRepository;
import com.example.virtualteacher.repository.contracts.RoleRepository;
import com.example.virtualteacher.repository.contracts.UserRepository;
import com.example.virtualteacher.services.contracts.UserService;
import com.example.virtualteacher.utils.AuthorisationChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static final String NOT_OWNER = "Only owners are allowed to modify their own profile information!";
    private static final String USER_DESIGNATED_AS_TEACHER = "This user is designated as a teacher in a course, therefore he/she cannot be deleted!";

    private final UserRepository userRepository;
    private final CourseRepository courseRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           CourseRepository courseRepository,
                           RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.courseRepository = courseRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public List<User> getAll(User requestingUser) {
        AuthorisationChecker.authorise(requestingUser);
        return userRepository.getAll();
    }

    @Override
    public User getById(int id, User requestingUser) {
        AuthorisationChecker.authorisedOrOwner(requestingUser,id);
            return userRepository.getById(id);

    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }


    @Override
    public List<User> search(String searchTerm, User requestingUser) {
       AuthorisationChecker.authorise(requestingUser);
        return userRepository.search(searchTerm);
    }

    @Override
    public void enroll(User user, int courseId) {
     AuthorisationChecker.checkIfStudent(user);
     Course courseToAdd = courseRepository.getById(courseId);
     User userToEnroll = userRepository.getById(user.getId());

        if (userToEnroll.getCourses().stream()
                .anyMatch(course -> course.getId() == courseToAdd.getId())) {
            throw new DuplicateEntityException("Course", "title", courseToAdd.getTitle());
        }

        userToEnroll.getCourses().add(courseToAdd);

        userRepository.update(userToEnroll);
    }

    @Override
    public void create(User user) {
        boolean duplicateExists = true;
        try {
            userRepository.getByField("email", user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
        userRepository.create(user);
    }

    @Override
    public void addRole(int userId, User admin, int roleId) {
        User userToAddRole = userRepository.getById(userId);
        AuthorisationChecker.authoriseAdmin(admin);
        if(userToAddRole.getRoles().stream().noneMatch(role -> role.getId()== roleId)){
            userToAddRole.getRoles().add(roleRepository.getById(roleId));
        }
        userRepository.update(userToAddRole);
    }


    @Override
    public void update(User user, User updatingUser) {
        if (updatingUser.getId() != user.getId()) {
            throw new UnauthorisedOperationException(NOT_OWNER);
        }
        userRepository.update(user);
    }

    @Override
    public void delete(int id, User updatingUser) {
        AuthorisationChecker.authoriseAdmin(updatingUser);

        for (Course course : courseRepository.getAll()) {
            if(course.getTeachers().stream().anyMatch(teacher ->teacher.getId()==id)){
                throw new InapplicableOperationException(USER_DESIGNATED_AS_TEACHER);
            }
        }
        userRepository.delete(id);
    }

}

