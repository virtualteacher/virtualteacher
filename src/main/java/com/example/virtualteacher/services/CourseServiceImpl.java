package com.example.virtualteacher.services;

import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.exceptions.InapplicableOperationException;
import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.repository.contracts.CourseRepository;
import com.example.virtualteacher.repository.contracts.StatusRepository;
import com.example.virtualteacher.repository.contracts.UserRepository;
import com.example.virtualteacher.services.contracts.CourseService;
import com.example.virtualteacher.utils.AuthorisationChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;
    private final StatusRepository statusRepository;
    private final UserRepository userRepository;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository,
                             StatusRepository statusRepository,
                             UserRepository userRepository) {
        this.courseRepository = courseRepository;
        this.statusRepository = statusRepository;
        this.userRepository = userRepository;
    }


    @Override
    public List<Course> getPublished()
    {
        List<Course> result = new ArrayList<>();
        for (Course course:courseRepository.getPublished()) {
            if(course.getStartingDate().isAfter(LocalDate.now()) || course.getStartingDate().isEqual(LocalDate.now())){
                result.add(course);
            }
        }
        return result;
    }

    @Override
    public List<Course> getAll(User user) {
       AuthorisationChecker.authorise(user);
        return courseRepository.getAll();
    }


    @Override
    public Course getById(int id) {
        return courseRepository.getById(id);
    }

    @Override
    public List<User> populateTeachers() {
        List<User> teachers = new ArrayList<>();
        for (User teacher: userRepository.getAll()) {
            if (teacher.getRoles().stream().anyMatch(role -> role.getId() == 2)) {
                teachers.add(teacher);
            }
        }
        return teachers;
    }


    @Override
    public List<Course> search(String searchTerm) {
        return courseRepository.search(searchTerm);
    }

    @Override
    public List<Course> searchAuthorised(String searchTerm) {
        return courseRepository.searchAuthorised(searchTerm);
    }

    @Override
    public List<Course> filter(Optional<String> title,
                               Optional<Integer> topicId,
                               Optional<Integer> teacherId,
                               Optional<Double> rating) {
        return courseRepository.filter(title,topicId,teacherId,rating);
    }

    @Override
    public List<Course> sort(Optional<String> title, Optional<String> rating) {
        return courseRepository.sort(title,rating);
    }

    @Override
    public void publish(Course course, User publisher) {
        AuthorisationChecker.authorise(publisher);
        course.setStatus(statusRepository.getById(2));
        courseRepository.update(course);
    }


    @Override
    public void create(Course course, User creatingUser) {
      AuthorisationChecker.authorise(creatingUser);
        for (User teacher : course.getTeachers()) {
          AuthorisationChecker.checkIfTeacher(teacher);
        }
        boolean duplicateExists = true;
        try {
            courseRepository.getByField("title", course.getTitle());
        } catch (
                EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Course", "title", course.getTitle());
        }
        courseRepository.create(course);
    }

    @Override
    public void update(Course course, User updatingUser) {
       AuthorisationChecker.authorise(updatingUser);
        for (User teacher : course.getTeachers()) {
        AuthorisationChecker.checkIfTeacher(teacher);
        }
        Course courseToUpdate = courseRepository.getById(course.getId());
        boolean duplicateTitleExist = true;
        try {
            if (!courseToUpdate.getTitle().equals(course.getTitle())) {
                courseRepository.getByField("title", course.getTitle());
            }
            if (courseToUpdate.getTitle().equals(course.getTitle())) {
                duplicateTitleExist = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateTitleExist = false;
        }
        if (duplicateTitleExist) {
            throw new DuplicateEntityException("Another course", "title", course.getTitle());
        }
        courseRepository.update(course);
    }

    @Override
    public void delete(int id, User deletingUser) {
        AuthorisationChecker.authoriseAdmin(deletingUser);

        for (User user : userRepository.getAll() ) {
            if(user.getCourses().stream().anyMatch(course ->course.getId()==id)){
                throw new InapplicableOperationException("There are enrolled users for the current course, therefore it cannot be deleted!");
            }
        }
        courseRepository.delete(id);
    }

}
