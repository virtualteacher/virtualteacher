package com.example.virtualteacher.services;

import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.exceptions.InapplicableOperationException;
import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.Lecture;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.repository.contracts.CourseRepository;
import com.example.virtualteacher.repository.contracts.LectureRepository;
import com.example.virtualteacher.services.contracts.LectureService;
import com.example.virtualteacher.utils.AuthorisationChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class LectureServiceImpl implements LectureService {

    private static final String LECTURE_ATTACHED_TO_COURSE = "This lecture appears in a least one course, therefore in cannot be deleted! Please make sure this lecture is detached from all courses it had appeared!";

    private final LectureRepository lectureRepository;
    private final CourseRepository courseRepository;

    @Autowired
    public LectureServiceImpl(LectureRepository lectureRepository, CourseRepository courseRepository) {
        this.lectureRepository = lectureRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public List<Lecture> getAll() {
        return lectureRepository.getAll();
    }

    @Override
    public Lecture getById(int id) {
        return lectureRepository.getById(id);
    }


    @Override
    public void create(Lecture lecture, User creatingUser) {
        AuthorisationChecker.authorise(creatingUser);
        boolean duplicateExists = true;
        try {
            lectureRepository.getByField("title", lecture.getTitle());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Lecture", "title", lecture.getTitle());
        }
        lectureRepository.create(lecture);
    }

    @Override
    public void update(Lecture lecture, User updatingUser) {
        AuthorisationChecker.authorise(updatingUser);
        Lecture lectureToUpdate = lectureRepository.getById(lecture.getId());
        boolean duplicateIdentifierExist = true;
        try {
            if (!lectureToUpdate.getTitle().equals(lecture.getTitle())) {
                lectureRepository.getByField("title", lecture.getTitle());
            }
            if (lectureToUpdate.getTitle().equals(lecture.getTitle())) {
                duplicateIdentifierExist = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateIdentifierExist = false;
        }
        if (duplicateIdentifierExist) {
            throw new DuplicateEntityException("Another lecture", "title", lecture.getTitle());
        }
        lectureRepository.update(lecture);

    }

    @Override
    public void attach(int lectureId, int courseId,User user) {
        AuthorisationChecker.authorise(user);
        Lecture lecture = lectureRepository.getById(lectureId);
        Course course = courseRepository.getById(courseId);
        if (course.getLectures().stream().anyMatch(l -> l.getTitle().equals(lecture.getTitle()))) {
            throw new DuplicateEntityException("Lecture", "title", lecture.getTitle());
        }
        course.getLectures().add(lecture);
        courseRepository.update(course);

    }

    @Override
    public void detach(int lectureId, int courseId, User user) {
        AuthorisationChecker.authorise(user);
        Course course = courseRepository.getById(courseId);
        if (course.getLectures().stream().noneMatch(l -> l.getId() == lectureId)) {
            throw new EntityNotFoundException("Lecture", "title", lectureRepository.getById(lectureId).getTitle());
        } else {
            Lecture lecture = course.getLectures().stream().filter(lecture1 -> lecture1.getId() == lectureId).findAny().get();
            course.getLectures().remove(lecture);
            courseRepository.update(course);
        }
    }


    @Override
    public void delete(int id, User deletingUser) {
       AuthorisationChecker.authoriseAdmin(deletingUser);
        for (Course course : courseRepository.getAll()) {
            if(course.getLectures().stream().anyMatch(lecture ->lecture.getId()==id)){
                throw new InapplicableOperationException(LECTURE_ATTACHED_TO_COURSE);
            }
        }
        lectureRepository.delete(id);
    }
}
