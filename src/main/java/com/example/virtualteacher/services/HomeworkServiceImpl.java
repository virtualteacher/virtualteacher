package com.example.virtualteacher.services;

import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.models.*;
import com.example.virtualteacher.repository.contracts.AssignmentRepository;
import com.example.virtualteacher.repository.contracts.HomeworkRepository;
import com.example.virtualteacher.services.contracts.AssignmentService;
import com.example.virtualteacher.services.contracts.HomeworkService;
import com.example.virtualteacher.utils.AuthorisationChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HomeworkServiceImpl implements HomeworkService {



    private final HomeworkRepository homeworkRepository;
    private final AssignmentRepository assignmentRepository;

    @Autowired
    public HomeworkServiceImpl(HomeworkRepository homeworkRepository,
                               AssignmentRepository assignmentRepository) {
        this.homeworkRepository = homeworkRepository;
        this.assignmentRepository = assignmentRepository;
    }

    @Override
    public List<Homework> getAll() {
        return homeworkRepository.getAll();
    }

    @Override
    public Homework getById(int id) {
        return homeworkRepository.getById(id);
    }

    @Override
    public void create(Homework homework, User creatingUser, Assignment assignment) {
        homework.setUserEmail(creatingUser.getEmail());
        homework.setAssignment(assignment);
        if(assignment.getHomeworks().stream().noneMatch(homework1 -> homework1.getUserEmail().equals(homework.getUserEmail()))){
            homeworkRepository.create(homework);
            assignment.getHomeworks().add(homework);

        }else {
            throw new DuplicateEntityException("Homework", "email", homework.getUserEmail());
        }
    }

    @Override
    public void evaluate(User teacher, int assignmentId, int homeworkId, double evaluation) {
        AuthorisationChecker.authorise(teacher);
            Assignment assignment = assignmentRepository.getById(assignmentId);
        Homework homeworkForEvaluation = assignment.getHomeworks()
                .stream()
                .filter(homework -> homework.getId() == homeworkId)
                .findAny()
                .get();
        homeworkForEvaluation.setEvaluation(evaluation);
        homeworkRepository.update(homeworkForEvaluation);
    }

}