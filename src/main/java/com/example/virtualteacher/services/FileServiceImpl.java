package com.example.virtualteacher.services;

import com.example.virtualteacher.repository.contracts.FileRepository;
import com.example.virtualteacher.services.contracts.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileServiceImpl implements FileService {

    private final FileRepository fileRepository;

    @Autowired
    public FileServiceImpl(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }
    @Override
    public String saveFile(MultipartFile multipartFile, String fileName) {
        return fileRepository.saveFile(multipartFile, fileName);
    }
    @Override
    public FileSystemResource getFile(String path) {
        return fileRepository.getFile(path);
    }

}
