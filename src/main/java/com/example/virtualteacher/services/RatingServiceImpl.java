package com.example.virtualteacher.services;

import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.Rating;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.repository.contracts.CourseRepository;
import com.example.virtualteacher.repository.contracts.RatingRepository;
import com.example.virtualteacher.services.contracts.RatingService;
import com.example.virtualteacher.utils.AuthorisationChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingServiceImpl implements RatingService {

    private final RatingRepository repository;
    private final CourseRepository courseRepository;

    @Autowired
    public RatingServiceImpl(RatingRepository repository,CourseRepository courseRepository) {
        this.repository = repository;
        this.courseRepository = courseRepository;
    }

    @Override
    public void rate(Course course, User user, double ratingValue) {
        Rating existingRecord;
        AuthorisationChecker.checkIfStudentEnrolledForCourse(user,course);
        try {
            existingRecord = repository.getByUserAndCourse(user, course);
            existingRecord.setRating(ratingValue);
            repository.update(existingRecord);
            course.setRating(getAverageRating(course));
            courseRepository.update(course);
        } catch (EntityNotFoundException e) {
            var rating = new Rating(user, course, ratingValue);
            repository.create(rating);
            course.setRating(getAverageRating(course));
            courseRepository.update(course);
        }
    }

    @Override
    public Double getAverageRating(Course course) {
        return repository.getAverage(course);
    }
}
