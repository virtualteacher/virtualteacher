package com.example.virtualteacher.repository.contracts;

import com.example.virtualteacher.models.User;

import java.util.List;

public interface UserRepository extends BaseModifyRepository<User>{

    List<User> search(String searchTerm);

    User getByEmail(String email);

}
