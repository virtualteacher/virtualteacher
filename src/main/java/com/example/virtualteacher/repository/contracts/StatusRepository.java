package com.example.virtualteacher.repository.contracts;

import com.example.virtualteacher.models.Status;

public interface StatusRepository extends BaseGetRepository<Status>{
}
