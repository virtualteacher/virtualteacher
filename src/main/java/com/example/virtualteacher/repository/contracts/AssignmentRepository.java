package com.example.virtualteacher.repository.contracts;

import com.example.virtualteacher.models.Assignment;

public interface AssignmentRepository extends BaseModifyRepository<Assignment>{
}
