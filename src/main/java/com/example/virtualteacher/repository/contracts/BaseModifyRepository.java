package com.example.virtualteacher.repository.contracts;

public interface BaseModifyRepository<T> extends BaseGetRepository<T> {
    void delete(int id);

    void update(T entity);

    void create(T entity);
}
