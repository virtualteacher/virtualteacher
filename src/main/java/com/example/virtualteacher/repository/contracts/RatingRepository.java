package com.example.virtualteacher.repository.contracts;

import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.Rating;
import com.example.virtualteacher.models.User;

public interface RatingRepository extends BaseModifyRepository<Rating>{

    Rating getByUserAndCourse(User user, Course course);

    Double getAverage(Course course);
}