package com.example.virtualteacher.repository.contracts;

import com.example.virtualteacher.models.Lecture;

public interface LectureRepository extends BaseModifyRepository<Lecture>{
}
