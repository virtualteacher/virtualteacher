package com.example.virtualteacher.repository.contracts;

import java.util.List;

public interface BaseGetRepository<T>{

    T getById(int id);

    List<T> getAll();

    T getByField(String fieldName, String fieldValue);


}

