package com.example.virtualteacher.repository.contracts;
import com.example.virtualteacher.models.Course;


import java.util.List;
import java.util.Optional;

public interface CourseRepository extends BaseModifyRepository<Course> {

    List<Course> getPublished();

    List<Course> search(String searchTerm);

    List<Course> searchAuthorised(String searchTerm);

    List<Course> filter(Optional<String> title,
                               Optional<Integer> topicId,
                               Optional<Integer> teacherId,
                               Optional<Double> rating);

    List<Course> sort(Optional<String> title, Optional<String> rating);
}
