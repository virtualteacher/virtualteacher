package com.example.virtualteacher.repository.contracts;

import com.example.virtualteacher.models.Topic;

public interface TopicRepository extends BaseGetRepository<Topic>{
}
