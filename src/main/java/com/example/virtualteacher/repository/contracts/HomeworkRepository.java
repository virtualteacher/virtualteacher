package com.example.virtualteacher.repository.contracts;

import com.example.virtualteacher.models.Homework;

public interface HomeworkRepository extends BaseModifyRepository<Homework>{
}
