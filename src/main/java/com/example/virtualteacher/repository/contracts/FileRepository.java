package com.example.virtualteacher.repository.contracts;

import org.springframework.core.io.FileSystemResource;
import org.springframework.web.multipart.MultipartFile;


public interface FileRepository {

    String saveFile(MultipartFile multipartFile, String fileName);

    FileSystemResource getFile(String path);

}
