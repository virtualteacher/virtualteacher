package com.example.virtualteacher.repository;

import com.example.virtualteacher.models.Assignment;
import com.example.virtualteacher.repository.contracts.AssignmentRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AssignmentRepositoryImpl extends BaseModifyRepositoryImpl<Assignment> implements AssignmentRepository {
    @Autowired
    public AssignmentRepositoryImpl(SessionFactory sessionFactory) {
        super(Assignment.class, sessionFactory);
    }
}
