package com.example.virtualteacher.repository;

import com.example.virtualteacher.repository.contracts.BaseModifyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


public abstract class BaseModifyRepositoryImpl<T> extends  BaseGetRepositoryImpl<T> implements BaseModifyRepository<T> {


    public BaseModifyRepositoryImpl(Class<T> clazz, SessionFactory sessionFactory) {
        super(clazz, sessionFactory);
    }

    @Override
    public void create(T entity) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(T entity) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        T toDelete = getById(id);
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.delete(toDelete);
            session.getTransaction().commit();
        }
    }
}
