package com.example.virtualteacher.repository;

import com.example.virtualteacher.models.Role;
import com.example.virtualteacher.models.Status;
import com.example.virtualteacher.repository.contracts.StatusRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StatusRepositoryImpl extends BaseGetRepositoryImpl<Status> implements StatusRepository {

    @Autowired
    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        super(Status.class, sessionFactory);

    }
}
