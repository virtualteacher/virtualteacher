package com.example.virtualteacher.repository;

import com.example.virtualteacher.models.Homework;
import com.example.virtualteacher.repository.contracts.HomeworkRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class HomeworkRepositoryImpl extends BaseModifyRepositoryImpl<Homework> implements HomeworkRepository {
    @Autowired
    public HomeworkRepositoryImpl(SessionFactory sessionFactory) {
        super(Homework.class, sessionFactory);
    }
}
