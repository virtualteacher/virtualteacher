package com.example.virtualteacher.repository;

import com.example.virtualteacher.repository.contracts.FileRepository;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
@Repository
public class FileRepositoryImpl implements FileRepository {

    private final Path root = Paths.get(".").normalize().toAbsolutePath();
    private final String RESOURCES_DIR = root + "/src/main/resources/static/uploads/";
    private final String OUTPUT_DIR = "/uploads/";
    @Override
    public String saveFile(MultipartFile file, String fileName) {
        String filename = new Date().getTime() + "_" + fileName;
        Path path = Paths.get(RESOURCES_DIR + filename);
        try {
            Files.createDirectories(path.getParent());
            Files.write(path, file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return OUTPUT_DIR + filename;
    }
    @Override
    public FileSystemResource getFile(String path) {
        try {
            return new FileSystemResource(Paths.get(RESOURCES_DIR + path));
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}