package com.example.virtualteacher.repository;

import com.example.virtualteacher.models.Topic;
import com.example.virtualteacher.repository.contracts.TopicRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TopicRepositoryImpl extends BaseGetRepositoryImpl<Topic> implements TopicRepository {

    @Autowired
    public TopicRepositoryImpl(SessionFactory sessionFactory) {
        super(Topic.class, sessionFactory);
    }
}
