package com.example.virtualteacher.repository;

import com.example.virtualteacher.models.Lecture;
import com.example.virtualteacher.repository.contracts.LectureRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LectureRepositoryImpl extends BaseModifyRepositoryImpl<Lecture> implements LectureRepository {

    @Autowired
    public LectureRepositoryImpl(SessionFactory sessionFactory) {
        super(Lecture.class, sessionFactory);
    }
}
