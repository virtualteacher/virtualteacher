package com.example.virtualteacher.repository;

import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.repository.contracts.CourseRepository;
import com.example.virtualteacher.repository.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class CourseRepositoryImpl extends BaseModifyRepositoryImpl<Course> implements CourseRepository {
    private final UserRepository userRepository;

    @Autowired
    public CourseRepositoryImpl(SessionFactory sessionFactory, UserRepository userRepository) {
        super(Course.class, sessionFactory);
        this.userRepository = userRepository;
    }

    @Override
    public List<Course> getPublished(){
        try (Session session = getSessionFactory().openSession()) {
            String hql = "from Course where status.id = 2";
            Query<Course> query = session.createQuery(hql, Course.class);
            if (query.list().size() == 0) {
                return new ArrayList<>();
            }
            return query.list();
        }
    }

    @Override
    public List<Course> search(String searchTerm) {
        try (Session session = getSessionFactory().openSession()) {
            String hql = "from Course where title like :searchTerm and status.id = 2 and startingDate >= current_date ";
            Query<Course> query = session.createQuery(hql, Course.class);
            query.setParameter("searchTerm", "%" + searchTerm + "%");
            if (query.list().size() == 0) {
                return new ArrayList<>();
            }
            return query.list();
        }

    }

    @Override
    public List<Course> searchAuthorised(String searchTerm) {
        try (Session session = getSessionFactory().openSession()) {
            String hql = "from Course where title like :searchTerm";
            Query<Course> query = session.createQuery(hql, Course.class);
            query.setParameter("searchTerm", "%" + searchTerm + "%");
            if (query.list().size() == 0) {
                return new ArrayList<>();
            }
            return query.list();
        }

    }

    @Override
    public List<Course> filter(Optional<String> title,
                               Optional<Integer> topicId,
                               Optional<Integer> teacherId,
                               Optional<Double> rating) {

        if (title.isPresent() && topicId.isEmpty() && teacherId.isEmpty() && rating.isEmpty()) {
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where title like :title and status.id = 2 and startingDate >= current_date", Course.class);
                query.setParameter("title", "%" + title.get() + "%");

                return query.list();
            }
        }

        if (topicId.isPresent() && title.isEmpty() && teacherId.isEmpty() && rating.isEmpty()) {
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where topic.id = :topicId and status.id = 2 and startingDate >= current_date", Course.class);
                query.setParameter("topicId", topicId.get());

                return query.list();
            }
        }

        if (teacherId.isPresent() && title.isEmpty() && topicId.isEmpty() && rating.isEmpty()) {
            User teacher = userRepository.getById(teacherId.get());
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where :teacher member of teachers and status.id = 2 and startingDate >= current_date", Course.class);
                query.setParameter("teacher", teacher);

                return query.list();
            }
        }

        if (rating.isPresent() && title.isEmpty() && topicId.isEmpty() && teacherId.isEmpty()) {
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where rating = :rating and status.id = 2 and startingDate >= current_date", Course.class);
                query.setParameter("rating", rating.get());

                return query.list();
            }
        }

        if (title.isPresent() && topicId.isPresent() && teacherId.isEmpty() && rating.isEmpty()) {
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where title like :title and topic.id = :topicId and status.id = 2 and startingDate >= current_date", Course.class);
                query.setParameter("title", "%" + title.get() + "%");
                query.setParameter("topicId", topicId.get());
                return query.list();
            }
        }

        if (topicId.isPresent() && title.isEmpty() && teacherId.isEmpty() && rating.isPresent()) {
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where rating = :rating and topic.id =:topicId and status.id = 2 and startingDate >= current_date", Course.class);
                query.setParameter("topicId", topicId.get());
                query.setParameter("rating", rating.get());
                return query.list();
            }
        }

        if (topicId.isEmpty() && title.isPresent() && teacherId.isPresent() && rating.isEmpty()) {
            User teacher = userRepository.getById(teacherId.get());
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where title like :title and :teacher member of teachers and status.id = 2 and startingDate >= current_date", Course.class);
                query.setParameter("title", "%" + title.get() + "%");
                query.setParameter("teacher", teacher);
                return query.list();
            }
        }

        if (topicId.isEmpty() && title.isEmpty() && teacherId.isPresent() && rating.isPresent()) {
            User teacher = userRepository.getById(teacherId.get());
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where rating = :rating and :teacher member of teachers and status.id = 2 and startingDate >= current_date", Course.class);
                query.setParameter("rating", rating.get());
                query.setParameter("teacher", teacher);
                return query.list();
            }
        }

        if (title.isPresent() && topicId.isEmpty() && teacherId.isEmpty() && rating.isPresent()) {
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where rating = :rating and title like :title and status.id = 2 and startingDate >= current_date", Course.class);
                query.setParameter("rating", rating.get());
                query.setParameter("title", "%" + title.get() + "%");

                return query.list();
            }
        }

        if (title.isEmpty() && topicId.isPresent() && teacherId.isPresent() && rating.isEmpty()) {
            User teacher = userRepository.getById(teacherId.get());
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where topic.id = :topicId and :teacher member of teachers and status.id = 2 and startingDate >= current_date", Course.class);
                query.setParameter("topicId", topicId.get());
                query.setParameter("teacher", teacher);
                return query.list();
            }
        }

        if (title.isPresent() && topicId.isPresent() && teacherId.isPresent() && rating.isEmpty()) {
            User teacher = userRepository.getById(teacherId.get());
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where topic.id = :topicId and :teacher member of teachers and title like :title and status.id = 2 and startingDate >= current_date", Course.class);
                query.setParameter("topicId", topicId.get());
                query.setParameter("teacher", teacher);
                query.setParameter("title", "%" + title.get() + "%");
                return query.list();
            }
        }

        if (title.isPresent() && topicId.isPresent() && rating.isPresent() && teacherId.isEmpty()) {
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where rating = :rating and topic.id = :topicId and title like :title and status.id = 2 and startingDate >= current_date", Course.class);
                query.setParameter("topicId", topicId.get());
                query.setParameter("title", "%" + title.get() + "%");
                query.setParameter("rating", rating.get());
                return query.list();
            }
        }

        if (title.isPresent() && topicId.isEmpty() && teacherId.isPresent() && rating.isPresent()) {
            User teacher = userRepository.getById(teacherId.get());
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where rating = :rating and :teacher member of teachers and title like :title and status.id = 2 and startingDate >= current_date", Course.class);
                query.setParameter("rating", rating.get());
                query.setParameter("teacher", teacher);
                query.setParameter("title", "%" + title.get() + "%");
                return query.list();
            }
        }

        if (title.isEmpty() && topicId.isPresent() && teacherId.isPresent() && rating.isPresent()) {
            User teacher = userRepository.getById(teacherId.get());
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where rating = :rating and topic.id = :topicId and :teacher member of teachers and status.id = 2 and startingDate >= current_date", Course.class);
                query.setParameter("topicId", topicId.get());
                query.setParameter("teacher", teacher);
                query.setParameter("rating", rating.get());
                return query.list();
            }
        }


        if (title.isPresent() && topicId.isPresent() && teacherId.isPresent() && rating.isPresent()) {
            User teacher = userRepository.getById(teacherId.get());
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where rating = :rating and topic.id = :topicId and :teacher member of teachers and title like :title and status.id = 2 and startingDate >= current_date", Course.class);
                query.setParameter("topicId", topicId.get());
                query.setParameter("teacher", teacher);
                query.setParameter("title", "%" + title.get() + "%");
                query.setParameter("rating", rating.get());
                return query.list();
            }
        }
        return getAll();
    }

    @Override
    public List<Course> sort(Optional<String> title, Optional<String> rating) {

        if (title.isPresent() && rating.isEmpty()) {
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where status.id = 2 and startingDate >= current_date order by title", Course.class);
                return query.list();
            }
        }
        if (rating.isPresent() && title.isEmpty()) {
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where status.id = 2 and startingDate >= current_date order by rating", Course.class);
                return query.list();
            }
        }
        if (title.isPresent() && rating.isPresent()) {
            try (Session session = getSessionFactory().openSession()) {
                Query<Course> query = session.createQuery("from Course where status.id = 2 and startingDate >= current_date order by title,rating", Course.class);
                return query.list();
            }
        }
        return getAll();
    }
}
