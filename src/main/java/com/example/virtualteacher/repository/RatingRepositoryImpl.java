package com.example.virtualteacher.repository;

import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.Rating;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.repository.contracts.RatingRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Repository
public class RatingRepositoryImpl  extends BaseModifyRepositoryImpl<Rating> implements RatingRepository {

    @Autowired
    public  RatingRepositoryImpl(SessionFactory sessionFactory) {
        super(Rating.class, sessionFactory);
    }

    @Override
    public Rating getByUserAndCourse(User user, Course course) {
        try (Session session = getSessionFactory().openSession()) {
            var query = session.createQuery("from Rating " +
                    "where user = :user and course = :course", Rating.class);

            query.setParameter("user", user);
            query.setParameter("course", course);

            var result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException(
                        String.format("Rating with user %s and course %s not found",
                                user.getEmail(), course.getTitle()));
            }

            return result.get(0);
        }
    }

    @Override
    public Double getAverage(Course course) {
        try (Session session = getSessionFactory().openSession()) {
            var query = session.createQuery("select avg(rating) " +
                    "from Rating where course = :course", Double.class);
            query.setParameter("course", course);

            Double result = query.uniqueResult();
          return BigDecimal.valueOf(result).setScale(1, RoundingMode.HALF_UP).doubleValue();

        }
    }
}