package com.example.virtualteacher.repository;

import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.repository.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepositoryImpl extends BaseModifyRepositoryImpl<User> implements UserRepository {

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory){
        super(User.class,sessionFactory);
    }

    @Override
    public List<User> search(String searchTerm) {
        try (Session session = getSessionFactory().openSession()) {
            String hql = "from User where email like :searchTerm";
            Query<User> query = session.createQuery(hql, User.class);
            query.setParameter("searchTerm","%"+searchTerm+"%");
            if (query.list().size() == 0) {
                return new ArrayList<>();
            }
            return query.list();
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = getSessionFactory().openSession()) {
            String hql = "from User where email = :email ";
            Query<User> query = session.createQuery(hql, User.class);
            query.setParameter("email", email);
            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            }

            return result.get(0);
        }
    }

}
