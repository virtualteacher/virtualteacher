package com.example.virtualteacher.utils;

import com.example.virtualteacher.models.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Helpers {

    public static User createMockTeacher(){
        User mockUser = new User();
        Role role = new Role(2, "Teacher");
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        mockUser.setId(2);
        mockUser.setFirstName("Boris");
        mockUser.setLastName("Grozev");
        mockUser.setEmail("test@mail.com");
        mockUser.setRoles(roles);

        return mockUser;

    }



    public static User createMockStudent(){
        User mockUser = new User();
        Role role = new Role(3, "Student");
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        mockUser.setId(2);
        mockUser.setFirstName("Boris");
        mockUser.setLastName("Grozev");
        mockUser.setEmail("test@mail.com");
        mockUser.setRoles(roles);

        return mockUser;

    }

    public static User createMockUser() {
        User mockUser = new User();
        Role role = new Role(1, "Administrator");
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        mockUser.setId(1);
        mockUser.setFirstName("Boris");
        mockUser.setLastName("Grozev");
        mockUser.setEmail("testuser@mail.com");
        mockUser.setRoles(roles);

        return mockUser;
    }

    public static Role createMockRole(String role) {
        Role mockRole = new Role();
        mockRole.setId(1);
        mockRole.setName(role);
        return mockRole;
    }

    public static Status createMockStatus(){
        Status status = new Status();
        status.setId(1);
        status.setType("Draft");

        return status;
    }

    public static Topic createMockTopic(){
        Topic topic = new Topic();
        topic.setId(1);
        topic.setName("Maths");

        return topic;
    }

    public static Lecture createMockLecture(){
        Lecture lecture = new Lecture();
        lecture.setId(1);
        lecture.setTitle("Lecture");
        lecture.setDescription("Description");
        lecture.setVideoUrl("videoUrl");
        return lecture;
    }

    public static Course createMockCourse(){
        Course course = new Course();
        course.setId(1);
        course.setTitle("Lecture");
        course.setDescription("Description");
        Set<User> teachers = new HashSet<>();
        teachers.add(createMockTeacher());
        course.setTeachers(teachers);
        course.setTopic(createMockTopic());
        course.setStatus(createMockStatus());
        return course;

    }

    public static Homework createMockHomework(){
        Homework homework = new Homework();
        homework.setId(1);
        homework.setName("Name");
        homework.setUserEmail("test@email.com");
        homework.setEvaluation(7.0);

        return homework;
    }

    public static Assignment createMockAssignment(){
        Assignment assignment = new Assignment();
        assignment.setId(1);
        assignment.setName("Name");
        List<Homework> homeworks = new ArrayList<>();
        homeworks.add(createMockHomework());
        assignment.setHomeworks(homeworks);

        return assignment;
    }



}
