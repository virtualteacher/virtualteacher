package com.example.virtualteacher.services;

import com.example.virtualteacher.models.Topic;
import com.example.virtualteacher.repository.contracts.TopicRepository;
import com.example.virtualteacher.utils.Helpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)

public class TopicServiceTests {

    @Mock
    TopicRepository mockRepository;

    @InjectMocks
    TopicServiceImpl service;

    @Test
    public void getById_Should_ReturnTopic_When_MatchExist() {
        // Arrange
        Topic topic = Helpers.createMockTopic();
        Mockito.when(mockRepository.getById(topic.getId())).thenReturn(topic);
        // Act
        Topic result = service.getById(1);

        // Assert
        Assertions.assertEquals(topic.getId(), result.getId());
        Assertions.assertEquals(topic.getName(), result.getName());

    }

    @Test
    public void getAll_Should_CallRepository() {
        //Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        //Act
        service.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();

    }
}