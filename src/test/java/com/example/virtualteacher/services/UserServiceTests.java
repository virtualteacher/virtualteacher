package com.example.virtualteacher.services;


import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.exceptions.InapplicableOperationException;
import com.example.virtualteacher.exceptions.UnauthorisedOperationException;
import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.repository.contracts.CourseRepository;
import com.example.virtualteacher.repository.contracts.UserRepository;
import com.example.virtualteacher.utils.Helpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@MockitoSettings(strictness = Strictness.LENIENT)
@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    UserRepository mockRepository;

    @Mock
    CourseRepository courseRepository;


    @InjectMocks
    UserServiceImpl service;

    @Test
    public void getById_Should_ReturnUser_When_MatchExist() {
        // Arrange
        User user = Helpers.createMockUser();
        Mockito.when(mockRepository.getById(user.getId()))
                .thenReturn(user);
        // Act
        User result = service.getById(1, user);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(user.getId(), result.getId()),
                () -> Assertions.assertEquals(user.getFirstName(),result.getFirstName()),
                () -> Assertions.assertEquals(user.getLastName(),result.getLastName()),
                () -> Assertions.assertEquals(user.getEmail(),result.getEmail())
        );

    }

    @Test
    public void getByEmail_Should_ReturnUser_When_MatchExist() {
        // Arrange
        User user = Helpers.createMockUser();
        Mockito.when(mockRepository.getByEmail(user.getEmail()))
                .thenReturn(user);
        // Act
        User result = service.getByEmail(user.getEmail());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(user.getEmail(),result.getEmail())
        );

    }


    @Test
    public void getAll_Should_CallRepository() {
        //Arrange
        User user = Helpers.createMockUser();
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        //Act
        service.getAll(user);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }


    @Test
    void search_should_callRepository() {
        // Arrange
        User user = Helpers.createMockUser();
        Mockito.when(mockRepository.search("searchTerm")).thenReturn(new ArrayList<>());

        // Act
        service.search("searchTerm", user);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .search("searchTerm");
    }

    @Test
    public void enroll_should_callRepository() {
        // Arrange
        User mockUser = Helpers.createMockStudent();

        Course course = Helpers.createMockCourse();

        Mockito.when(courseRepository.getById(course.getId()))
                .thenReturn(course);

        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        Set<Course> courses = new HashSet<>();
        //courses.add(course);

        mockUser.setCourses(courses);

        // Act
        service.enroll(mockUser, course.getId());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    public void enroll_should_throwException_whenAlreadyEnrolled() {
        // Arrange
        User mockUser = Helpers.createMockStudent();

        Course course = Helpers.createMockCourse();

        Mockito.when(courseRepository.getById(course.getId()))
                .thenReturn(course);

        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        Set<Course> courses = new HashSet<>();
        courses.add(course);

        mockUser.setCourses(courses);

        // Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.enroll(mockUser, course.getId()));

    }



    @Test
    public void addRole_should_callRepository() {
        // Arrange
        User mockUser = Helpers.createMockStudent();

        User user = Helpers.createMockUser();

        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        // Act
        service.addRole(mockUser.getId(), user, 3);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    public void create_should_callRepository_when_userWithSameEmailDoesNotExist() {
        // Arrange
        User mockUser = Helpers.createMockStudent();

        Mockito.when(mockRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockUser);
    }

    @Test
    public void create_should_throwException_when_userWithSameEmailExist() {
        // Arrange
        User mockUser = Helpers.createMockStudent();

        Mockito.when(mockRepository.getByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        //Act & Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> {
            service.create(mockUser);
        });
    }

    @Test
    public void update_should_throwException_when_userIsNotEmployee() {
        // Arrange
        User mockInitiator = Helpers.createMockStudent();
        User mockUser = Helpers.createMockUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorisedOperationException.class,
                () -> service.update(mockUser, mockInitiator));
    }

    @Test
    public void update_should_callRepository_when_userIsAuthorised() {
        // Arrange
        User mockInitiator = Helpers.createMockUser();
        User mockUser = Helpers.createMockUser();
        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);
        // Act
        service.update(mockUser, mockInitiator);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    public void update_should_throwException_when_updatedUserEmailExist() {
        // Arrange
        User userToUpdate = Helpers.createMockUser();
        userToUpdate.setEmail("testuser@mail.com");


        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(userToUpdate);


        // Act & Assert
        Assertions.assertThrows(UnauthorisedOperationException.class,
                () -> service.update(userToUpdate, Helpers.createMockStudent()));
    }

    @Test
    void delete_should_callRepository_when_initiatorIsAdmin() {
        // Arrange
        User mockUser = Helpers.createMockStudent();
        User mockInitiator = Helpers.createMockUser();

        Mockito.when(courseRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.delete(mockUser.getId(), mockInitiator);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockUser.getId());
    }

    @Test
    void delete_should_throwException_when_initiatorIsNotEmployee() {
        // Arrange
        User mockUser = Helpers.createMockUser();
        User mockInitiator = Helpers.createMockStudent();

        // Act & Assert
        Assertions.assertThrows(UnauthorisedOperationException.class, () -> service.delete(mockUser.getId(), mockInitiator));
    }

    @Test
    void delete_should_throwException_when_userAreEnrolled() {
        // Arrange
        Course course = Helpers.createMockCourse();
        User user = Helpers.createMockUser();

        User teacher = Helpers.createMockTeacher();

        List<Course> courses = new ArrayList<>();
        courses.add(course);


        Set<User> teachers = new HashSet<>();
        teachers.add(user);

        course.setTeachers(teachers);

        Mockito.when(courseRepository.getAll())
                .thenReturn((List<Course>) courses);

        // Act & Assert
        Assertions.assertThrows(InapplicableOperationException.class, () -> service.delete(course.getId(), user));
    }

}

