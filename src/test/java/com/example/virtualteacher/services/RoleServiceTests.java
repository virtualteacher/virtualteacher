package com.example.virtualteacher.services;

import com.example.virtualteacher.models.Role;
import com.example.virtualteacher.repository.contracts.RoleRepository;
import com.example.virtualteacher.utils.Helpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class RoleServiceTests {

    @Mock
    RoleRepository mockRepository;

    @InjectMocks
    RoleServiceImpl service;
    @Test
    public void getById_Should_ReturnRole_When_MatchExist() {
        // Arrange
        Role role = Helpers.createMockRole("Student");
        Mockito.when(mockRepository.getById(role.getId()))
                .thenReturn(role);
        // Act
        Role result = service.getById(role.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(role.getId(), result.getId()),
                () -> Assertions.assertEquals(role.getName(), result.getName())
        );
    }

    @Test
    public void getAll_Should_CallRepository() {
        //Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        //Act
        service.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }
}
