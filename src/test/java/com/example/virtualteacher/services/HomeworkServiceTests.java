package com.example.virtualteacher.services;


import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.models.*;
import com.example.virtualteacher.repository.contracts.*;
import com.example.virtualteacher.utils.Helpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.*;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class HomeworkServiceTests {

    @Mock
    HomeworkRepository mockRepository;

    @Mock
    UserRepository userRepository;

    @Mock
    StatusRepository statusRepository;

    @InjectMocks
    HomeworkServiceImpl service;

    @Test
    public void getById_Should_ReturnUser_When_MatchExist() {
        // Arrange
        Homework homework = Helpers.createMockHomework();
        Mockito.when(mockRepository.getById(homework.getId()))
                .thenReturn(homework);
        // Act
        Homework result = service.getById(1);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(homework.getId(), result.getId()),
                () -> Assertions.assertEquals(homework.getName(),result.getName())
        );

    }



    @Test
    public void getAll_Should_CallRepository() {
        //Arrange
        Assignment assignment = Helpers.createMockAssignment();
        User user = Helpers.createMockUser();

        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        //Act
        service.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void create_should_callRepository_when_homeworkWhenEmailDoesNotExist() {
        // Arrange
        Homework homework = Helpers.createMockHomework();
        Assignment assignment = Helpers.createMockAssignment();
        User user = Helpers.createMockTeacher();



        Mockito.when(mockRepository.getByField("name", homework.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(homework, user, assignment);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(homework);
    }

    @Test
    public void create_should_throwException_when_homeworkWhenEmailExist() {
        // Arrange
        Homework homework = Helpers.createMockHomework();
        Assignment assignment = Helpers.createMockAssignment();
        User user = Helpers.createMockTeacher();

        service.create(homework, user, assignment);

        //Act & Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> {
            service.create(homework, user, assignment);
        });
    }

}

