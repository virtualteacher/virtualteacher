package com.example.virtualteacher.services;


import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.exceptions.InapplicableOperationException;
import com.example.virtualteacher.exceptions.UnauthorisedOperationException;
import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.Status;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.repository.contracts.CourseRepository;
import com.example.virtualteacher.repository.contracts.StatusRepository;
import com.example.virtualteacher.repository.contracts.UserRepository;
import com.example.virtualteacher.utils.Helpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.*;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class CourseServiceTests {

    @Mock
    CourseRepository mockRepository;

    @Mock
    UserRepository userRepository;

    @Mock
    StatusRepository statusRepository;

    @InjectMocks
    CourseServiceImpl service;

    @Test
    public void getById_Should_ReturnUser_When_MatchExist() {
        // Arrange
        Course course = Helpers.createMockCourse();
        Mockito.when(mockRepository.getById(course.getId()))
                .thenReturn(course);
        // Act
        Course result = service.getById(1);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(course.getId(), result.getId()),
                () -> Assertions.assertEquals(course.getTitle(),result.getTitle()),
                () -> Assertions.assertEquals(course.getDescription(), result.getDescription()),
                () -> Assertions.assertEquals(course.getTopic(),result.getTopic())
        );

    }



    @Test
    public void getAll_Should_CallRepository() {
        //Arrange
        Course course = Helpers.createMockCourse();
        User user = Helpers.createMockUser();

        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        //Act
        service.getAll(user);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getPublished_Should_CallRepository() {
        //Arrange
        Course course = Helpers.createMockCourse();
        User user = Helpers.createMockUser();
        Mockito.when(mockRepository.getPublished())
                .thenReturn(new ArrayList<>());
        //Act
        service.getPublished();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getPublished();
    }


    @Test
    void search_should_callRepository() {
        // Arrange
        User user = Helpers.createMockUser();
        Mockito.when(mockRepository.search("searchTerm")).thenReturn(new ArrayList<>());

        // Act
        service.search("searchTerm");

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .search("searchTerm");
    }

    @Test
    void searchAuthorised_should_callRepository() {
        // Arrange
        User user = Helpers.createMockUser();
        Mockito.when(mockRepository.searchAuthorised("searchTerm")).thenReturn(new ArrayList<>());

        // Act
        service.searchAuthorised("searchTerm");

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .searchAuthorised("searchTerm");
    }


    @Test
    public void publish_should_callRepository() {
        // Arrange
        Course course = Helpers.createMockCourse();
        User user = Helpers.createMockTeacher();

        Mockito.when(statusRepository.getById(2))
                .thenReturn(new Status(2, "Published"));


        // Act
        service.publish(course, user);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(course);
    }

    @Test
    void filter_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.filter(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.filter(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Test
    void sort_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.sort(Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.sort(Optional.empty(), Optional.empty());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .sort(Optional.empty(), Optional.empty());
    }


    @Test
    public void create_should_callRepository_when_courseWithSameTitleDoesNotExist() {
        // Arrange
        Course course = Helpers.createMockCourse();
        User user = Helpers.createMockTeacher();



        Mockito.when(mockRepository.getByField("title", course.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(course, user);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(course);
    }

    @Test
    public void create_should_throwException_when_courseWithSameTitleExist() {
        // Arrange
        Course course = Helpers.createMockCourse();
        User user = Helpers.createMockTeacher();

        Mockito.when(mockRepository.getByField("title", course.getTitle()))
                .thenReturn(course);

        //Act & Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> {
            service.create(course, user);
        });
    }


    @Test
    public void update_should_throwException_when_userIsNotTeacher() {
        // Arrange
        Course course = Helpers.createMockCourse();
        User user = Helpers.createMockStudent();
        Mockito.when(mockRepository.getByField("title", course.getTitle()))
                .thenReturn(course);

        // Act, Assert
        Assertions.assertThrows(UnauthorisedOperationException.class,
                () -> service.update(course, user));
    }

    @Test
    public void update_should_callRepository_when_userIsTeacher() {
        // Arrange
        Course course = Helpers.createMockCourse();;
        User user = Helpers.createMockTeacher();

        Mockito.when(mockRepository.getById(course.getId()))
                .thenReturn(course);

        // Act
        service.update(course, user);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(course);
    }

    @Test
    public void update_should_throwException_when_updatedCourseTitleExist() {
        // Arrange
        Course course = Helpers.createMockCourse();
        Course courseToUpdate = Helpers.createMockCourse();
        courseToUpdate.setTitle("Title");
        User user = Helpers.createMockTeacher();

        Mockito.when(mockRepository.getById(course.getId()))
                .thenReturn(courseToUpdate);

        Mockito.when(mockRepository.getByField("title", course.getTitle()))
                .thenReturn(course);

        // Act & Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(course, user));
    }

    @Test
    public void update_should_throwException_when_updatedCourseDoseNotExist() {
        // Arrange
        Course course = Helpers.createMockCourse();
        Course courseToUpdate = Helpers.createMockCourse();
        courseToUpdate.setTitle("Title");
        User user = Helpers.createMockTeacher();

        Mockito.when(mockRepository.getById(course.getId()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByField("title", course.getTitle()))
                .thenReturn(course);

        // Act & Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.update(course, user));
    }


    @Test
    void delete_should_callRepository_when_initiatorIsTeacher() {
        // Arrange
        Course course = Helpers.createMockCourse();
        User user = Helpers.createMockUser();

        Mockito.when(userRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.delete(course.getId(), user);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(course.getId());
    }

    @Test
    void delete_should_throwException_when_initiatorIsNotAdmin() {
        // Arrange
        Course course = Helpers.createMockCourse();
        User user = Helpers.createMockStudent();


        // Act & Assert
        Assertions.assertThrows(UnauthorisedOperationException.class, () -> service.delete(course.getId(), user));
    }

    @Test
    void delete_should_throwException_when_userAreEnrolled() {
        // Arrange
        Course course = Helpers.createMockCourse();
        User user = Helpers.createMockUser();
        Set<Course> courses = new HashSet<>();
        courses.add(course);
        user.setCourses(courses);

        List<User> users = new ArrayList<>();
        users.add(user);

        Mockito.when(userRepository.getAll())
                .thenReturn(users);

        // Act & Assert
        Assertions.assertThrows(InapplicableOperationException.class, () -> service.delete(course.getId(), user));
    }



}

