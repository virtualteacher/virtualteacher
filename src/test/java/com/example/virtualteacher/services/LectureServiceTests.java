package com.example.virtualteacher.services;

import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.exceptions.InapplicableOperationException;
import com.example.virtualteacher.exceptions.UnauthorisedOperationException;
import com.example.virtualteacher.models.Course;
import com.example.virtualteacher.models.Lecture;
import com.example.virtualteacher.models.User;
import com.example.virtualteacher.repository.contracts.CourseRepository;
import com.example.virtualteacher.repository.contracts.LectureRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.example.virtualteacher.utils.Helpers.*;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class LectureServiceTests {
    @Mock
    LectureRepository mockRepository;
    @Mock
    CourseRepository courseRepository;

    @InjectMocks
    LectureServiceImpl service;
    @Test
    public void getById_Should_ReturnLecture_When_MatchExist() {
        // Arrange
        Lecture lecture = createMockLecture();
        Mockito.when(mockRepository.getById(lecture.getId()))
                .thenReturn(lecture);
        // Act
        Lecture result = service.getById(1);
        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(lecture.getId(), result.getId()),
                () -> Assertions.assertEquals(lecture.getTitle(), result.getTitle()),
                () -> Assertions.assertEquals(lecture.getDescription(), result.getDescription()),
                () -> Assertions.assertEquals(lecture.getVideoUrl(), result.getVideoUrl())
        );

    }
    @Test
    public void getAll_Should_CallRepository() {
        //Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        //Act
        service.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }


    @Test
    public void create_should_callRepository_when_LectureWithSameTitleDoesNotExist() {
        // Arrange
        User user = createMockUser();

        Lecture lecture = createMockLecture();

        Mockito.when(mockRepository.getByField("title",lecture.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(lecture, user);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(lecture);
    }

    @Test
    public void create_should_throwException_when_LectureWithSameTitleExist() {
        // Arrange
        User user = createMockUser();

        Lecture lecture = createMockLecture();

        Mockito.when(mockRepository.getByField("title",lecture.getTitle()))
                .thenReturn(lecture);

        //Act & Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> {
            service.create(lecture, user);
        });
    }

    @Test
    public void create_should_throwException_when_whenUserIsNotAuthorised() {
        // Arrange
        User mockInitiator = createMockStudent();
        Lecture lecture = createMockLecture();

        //Act & Assert
        Assertions.assertThrows(UnauthorisedOperationException.class, () -> {
            service.create(lecture, mockInitiator);
        });
    }

    @Test
    public void update_should_throwException_when_userIsNotAuthorised() {
        // Arrange
        User mockInitiator = createMockStudent();
        Lecture lecture = createMockLecture();

        // Act, Assert
        Assertions.assertThrows(UnauthorisedOperationException.class,
                () -> service.update(lecture, mockInitiator));
    }


    @Test
    public void update_should_callRepository_when_userIsAuthorised() {
        // Arrange
        User mockInitiator = createMockUser();
        Lecture lecture = createMockLecture();
        Mockito.when(mockRepository.getById(lecture.getId()))
                .thenReturn(lecture);
        // Act
        service.update(lecture, mockInitiator);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(lecture);
    }

    @Test
    public void update_should_throwException_when_LectureWithSameTitleExist() {
        // Arrange
        User mockInitiator = createMockUser();
        Lecture lectureToUpdate = createMockLecture();
        lectureToUpdate.setTitle("Lecture");

        Lecture  lectureDto = createMockLecture();
        lectureDto.setId(1);
        lectureDto.setTitle("Lecture2");

        Lecture existingLectureWithSameTitle = createMockLecture();
        existingLectureWithSameTitle.setTitle("Lecture2");

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(lectureToUpdate);

        Mockito.when(mockRepository.getByField("title",lectureDto.getTitle()))
                .thenReturn(lectureDto);

        // Act & Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(lectureDto, mockInitiator));
    }

    @Test
    public void update_should_throwException_when_LectureDoesNotExist() {
        // Arrange
        Lecture lecture = createMockLecture();
        Lecture lectureToUpdate = createMockLecture();
        lectureToUpdate.setTitle("Title");
        User user = createMockUser();
        Mockito.when(mockRepository.getById(lecture.getId()))
                .thenThrow(EntityNotFoundException.class);
        // Act & Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.update(lecture, user));
    }

    @Test
    void delete_should_callRepository_when_initiatorIsAdmin() {
        // Arrange
        Lecture lecture = createMockLecture();
        User mockInitiator = createMockUser();
        Mockito.when(courseRepository.getAll())
                .thenReturn(new ArrayList<>());
        // Act
        service.delete(lecture.getId(), mockInitiator);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(lecture.getId());
    }

    @Test
    void delete_should_throwException_when_initiatorIsNotAdmin() {
        // Arrange
        Lecture lecture = createMockLecture();
        User mockInitiator = createMockStudent();
        // Act & Assert
        Assertions.assertThrows(UnauthorisedOperationException.class, () -> service.delete(lecture.getId(), mockInitiator));
    }

    @Test
    void delete_should_throwException_when_lectureIsAttachedToCourse() {
        // Arrange
        User mockInitiator = createMockUser();
        Lecture lecture = createMockLecture();
        Course course = createMockCourse();
        Set<Lecture> lectures = new HashSet<>();
        lectures.add(lecture);
        course.setLectures(lectures);
        List<Course> courses = new ArrayList<>();
        courses.add(course);
        Mockito.when(courseRepository.getAll())
                .thenReturn(courses);
        // Act & Assert
        Assertions.assertThrows(InapplicableOperationException.class, () -> service.delete(lecture.getId(), mockInitiator));
    }


    @Test
    void attach_should_throwException_when_initiatorIsNotAuthorised() {
        // Arrange
        Lecture lecture = createMockLecture();
        Course course = createMockCourse();
        User mockInitiator = createMockStudent();

        // Act & Assert
        Assertions.assertThrows(UnauthorisedOperationException.class, () -> service.attach(lecture.getId(),course.getId(),mockInitiator));
    }

    @Test
    void attach_should_callRepository_when_initiatorIsAuthorised() {
        // Arrange
        Lecture lecture = createMockLecture();
        Course course = createMockCourse();
        User mockInitiator = createMockUser();
        Mockito.when(mockRepository.getById(lecture.getId()))
                .thenReturn(lecture);
        Mockito.when(courseRepository.getById(course.getId()))
                .thenReturn(course);
        Set<Lecture> lectures = new HashSet<>();
        course.setLectures(lectures);

        // Act
        service.attach(lecture.getId(),course.getId(),mockInitiator);

        // Assert
        Mockito.verify(courseRepository, Mockito.times(1))
                .update(course);

    }

    @Test
    void attach_should_throw_when_lectureAlreadyAttachedToCourse() {
        // Arrange
        Lecture lecture = createMockLecture();
        Course course = createMockCourse();
        User mockInitiator = createMockUser();
        Mockito.when(mockRepository.getById(lecture.getId()))
                .thenReturn(lecture);
        Mockito.when(courseRepository.getById(course.getId()))
                .thenReturn(course);
        Set<Lecture> lectures = new HashSet<>();
        lectures.add(lecture);
        course.setLectures(lectures);

        //Act & Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.attach(lecture.getId(),course.getId(),mockInitiator));


    }

    @Test
    void detach_should_throwException_when_initiatorIsNotAuthorised() {
        // Arrange
        Lecture lecture = createMockLecture();
        Course course = createMockCourse();
        User mockInitiator = createMockStudent();

        // Act & Assert
        Assertions.assertThrows(UnauthorisedOperationException.class, () -> service.detach(lecture.getId(),course.getId(),mockInitiator));
    }

    @Test
    void detach_should_callRepository_when_initiatorIsAuthorised() {
        // Arrange
        Lecture lecture = createMockLecture();
        Course course = createMockCourse();
        User mockInitiator = createMockUser();
        Mockito.when(mockRepository.getById(lecture.getId()))
                .thenReturn(lecture);
        Mockito.when(courseRepository.getById(course.getId()))
                .thenReturn(course);
        Set<Lecture> lectures = new HashSet<>();
        lectures.add(lecture);
        course.setLectures(lectures);
        //Act
        service.detach(lecture.getId(),course.getId(),mockInitiator);

        // Assert
        Mockito.verify(courseRepository, Mockito.times(1))
                .update(course);

    }

    @Test
    void detach_should_throw_when_lectureNeverAttachedToCourse() {
        // Arrange
        Lecture lecture = createMockLecture();
        Course course = createMockCourse();
        User mockInitiator = createMockUser();
        Mockito.when(mockRepository.getById(lecture.getId()))
                .thenReturn(lecture);
        Mockito.when(courseRepository.getById(course.getId()))
                .thenReturn(course);
        Set<Lecture> lectures = new HashSet<>();
        course.setLectures(lectures);
        //Act & Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> service.detach(lecture.getId(),course.getId(),mockInitiator));


    }


}
