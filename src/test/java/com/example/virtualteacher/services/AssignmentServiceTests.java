package com.example.virtualteacher.services;


import com.example.virtualteacher.exceptions.DuplicateEntityException;
import com.example.virtualteacher.exceptions.EntityNotFoundException;
import com.example.virtualteacher.exceptions.InapplicableOperationException;
import com.example.virtualteacher.exceptions.UnauthorisedOperationException;
import com.example.virtualteacher.models.*;
import com.example.virtualteacher.repository.contracts.*;
import com.example.virtualteacher.services.contracts.AssignmentService;
import com.example.virtualteacher.utils.Helpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.*;

import static com.example.virtualteacher.utils.Helpers.*;
import static com.example.virtualteacher.utils.Helpers.createMockUser;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class AssignmentServiceTests {

    @Mock
    AssignmentRepository mockRepository;

    @Mock
    LectureRepository lectureRepository;

    @Mock
    StatusRepository statusRepository;

    @InjectMocks
    AssignmentServiceImpl service;

    @Test
    public void getById_Should_ReturnUser_When_MatchExist() {
        // Arrange
        Assignment assignment = Helpers.createMockAssignment();
        Mockito.when(mockRepository.getById(assignment.getId()))
                .thenReturn(assignment);
        // Act
        Assignment result = service.getById(1);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(assignment.getId(), result.getId()),
                () -> Assertions.assertEquals(assignment.getName(), result.getName())
        );

    }


    @Test
    public void getAll_Should_CallRepository() {
        //Arrange
        Assignment assignment = Helpers.createMockAssignment();
        User user = Helpers.createMockUser();

        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        //Act
        service.getAll(user);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void create_should_callRepository_when_assignmentWithSameNameDoesNotExist() {
        // Arrange
        Assignment assignment = Helpers.createMockAssignment();
        User user = Helpers.createMockTeacher();


        Mockito.when(mockRepository.getByField("name", assignment.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(assignment, user);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(assignment);
    }

    @Test
    public void create_should_throwException_when_assignmentWithSameNameExist() {
        // Arrange
        Assignment assignment = Helpers.createMockAssignment();
        User user = Helpers.createMockTeacher();

        Mockito.when(mockRepository.getByField("name", assignment.getName()))
                .thenReturn(assignment);

        //Act & Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> {
            service.create(assignment, user);
        });
    }

    @Test
    void attach_should_throwException_when_initiatorIsNotAuthorised() {
        // Arrange
        Lecture lecture = createMockLecture();
        Assignment assignment = createMockAssignment();
        User mockInitiator = createMockStudent();

        // Act & Assert
        Assertions.assertThrows(UnauthorisedOperationException.class, () -> service.attach(assignment.getId(), lecture.getId(), mockInitiator));
    }

    @Test
    void attach_should_callRepository_when_initiatorIsAuthorised() {
        // Arrange
        Assignment assignment = createMockAssignment();
        Lecture lecture = createMockLecture();
        User mockInitiator = createMockUser();

        Mockito.when(mockRepository.getById(assignment.getId()))
                .thenReturn(assignment);

        Mockito.when(lectureRepository.getById(lecture.getId()))
                .thenReturn(lecture);

        Set<Assignment> assignments = new HashSet<>();
        lecture.setAssignments(assignments);

        // Act
        service.attach(assignment.getId(), lecture.getId(), mockInitiator);

        // Assert
        Mockito.verify(lectureRepository, Mockito.times(1))
                .update(lecture);

    }

    @Test
    void attach_should_throw_when_lectureAlreadyAttachedToCourse() {
        // Arrange
        Assignment assignment = createMockAssignment();
        Lecture lecture = createMockLecture();
        User mockInitiator = createMockUser();

        Mockito.when(mockRepository.getById(assignment.getId()))
                .thenReturn(assignment);

        Mockito.when(lectureRepository.getById(lecture.getId()))
                .thenReturn(lecture);

        Set<Assignment> assignments = new HashSet<>();
        assignments.add(assignment);
        lecture.setAssignments(assignments);

        //Act & Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.attach(assignment.getId(), lecture.getId(), mockInitiator));


    }


    @Test
    void detach_should_throwException_when_initiatorIsNotAuthorised() {
        // Arrange
        Lecture lecture = createMockLecture();
        Assignment assignment = createMockAssignment();
        User mockInitiator = createMockStudent();

        // Act & Assert
        Assertions.assertThrows(UnauthorisedOperationException.class, () -> service.detach(assignment.getId(), lecture.getId(), mockInitiator));
    }

    @Test
    void detach_should_callRepository_when_initiatorIsAuthorised() {
        // Arrange
        Assignment assignment = createMockAssignment();
        Lecture lecture = createMockLecture();
        User mockInitiator = createMockUser();

        Mockito.when(mockRepository.getById(assignment.getId()))
                .thenReturn(assignment);

        Mockito.when(lectureRepository.getById(lecture.getId()))
                .thenReturn(lecture);

        Set<Assignment> assignments = new HashSet<>();
        assignments.add(assignment);
        lecture.setAssignments(assignments);

        //Act
        service.detach(assignment.getId(), lecture.getId(), mockInitiator);

        // Assert
        Mockito.verify(lectureRepository, Mockito.times(1))
                .update(lecture);

    }

    @Test
    void detach_should_throw_when_lectureNeverAttachedToCourse() {
        // Arrange

        Assignment assignment = createMockAssignment();
        Lecture lecture = createMockLecture();
        User mockInitiator = createMockUser();

        Mockito.when(mockRepository.getById(assignment.getId()))
                .thenReturn(assignment);

        Mockito.when(lectureRepository.getById(lecture.getId()))
                .thenReturn(lecture);

        Set<Assignment> assignments = new HashSet<>();
        lecture.setAssignments(assignments);

        //Act & Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> service.detach(assignment.getId(), lecture.getId(), mockInitiator));

    }
}

