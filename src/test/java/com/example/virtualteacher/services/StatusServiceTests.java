package com.example.virtualteacher.services;

import com.example.virtualteacher.models.Status;
import com.example.virtualteacher.repository.contracts.StatusRepository;
import com.example.virtualteacher.utils.Helpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)

public class StatusServiceTests {

    @Mock
    StatusRepository mockRepository;

    @InjectMocks
    StatusServiceImpl service;

    @Test
    public void getById_Should_ReturnStatus_When_MatchExist() {
        // Arrange
        Status status = Helpers.createMockStatus();
        Mockito.when(mockRepository.getById(status.getId())).thenReturn(status);
        // Act
        Status result = service.getById(1);

        // Assert
        Assertions.assertEquals(status.getId(), result.getId());
        Assertions.assertEquals(status.getType(), result.getType());

    }

    @Test
    public void getAll_Should_CallRepository() {
        //Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        //Act
        service.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();

    }
}
